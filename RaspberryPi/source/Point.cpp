


#include "/home/arack/git/hexapod-robot/RaspberryPi/header/Point.h"
#include "/home/arack/git/hexapod-robot/RaspberryPi/header/CommonResource.h"
#include "/home/arack/git/hexapod-robot/RaspberryPi/header/Kinematic.h"
#include <cmath>
#include <iostream>

Point::Point(double x, double y, double z)
{
    this->x = x;
    this->y = y;
    this->z = z;
}
Point * Point::translateX (double extend )
{
    
    Point * newPoint = new Point  (this->x+extend, this->y , this->z);
    return newPoint; 
}
Point * Point::translateY (double extend )
{
    Point * newPoint = new Point  (this->x , this->y+extend , this->z);
    return newPoint;
}
void Point::translateZ( double extend )
{
    this->z = -1*Tibia_Length - extend;
}

Point* Point::coxaRotate(double angle)
{
    double newX = this->x*cos(angle*PI/180.0) - this->y*sin(angle*PI/180.0);
    double newY = this->x*sin(angle*PI/180.0) + this->y*cos(angle*PI/180.0);
    Point * newPoint = new Point ( newX, newY, z);
    return newPoint;
}

Point* Point::femurRotate(double angle)
{
    double newX;
    double newY;
    double newZ;
    
    double legLength = sqrt( pow(this->x,2.0) + pow(this->y,2.0) );
    
    if ( this->y > 0 )
    {
       newY = (legLength-Coxa_Length)*cos(angle*PI/180.0) - this->z*sin(angle*PI/180.0)+Coxa_Length;
       if ( newY < 0 )
       {
           cout << "Error: Y pos over the limit, invalid pos" << endl;
           return this;
       }
       newZ = (legLength-Coxa_Length)*sin(angle*PI/180.0) + this->z*cos(angle*PI/180.0);

    }
    else if ( this->y < 0 )
    {
        newY = (-1*legLength+Coxa_Length)*cos(angle*PI/180.0) - this->z*sin(angle*PI/180.0)-Coxa_Length;
        if ( newY > 0 )
        {
           cout << "Error: Y pos over the limit, invalid pos" << endl;
           return this;
        }
        newZ = (-1*legLength+Coxa_Length)*sin(angle*PI/180.0) + this->z*cos(angle*PI/180.0);

    }
    double originalCoxaAngle = atan2(abs(this->x),abs(this->y));
    newX = newY * sin(originalCoxaAngle);
    newY = newY * cos(originalCoxaAngle);
    //double newX = newY/tan(originalCoxaAngle);
    if ( this->x >= 0 )
    {
        newX = abs(newX);
    }
    else 
    {
        newX = -1*abs(newX);
    }
    
    
    Point * newPoint = new Point (newX, newY, newZ);
    return newPoint;
    //int deg1,deg2,deg3;
    //Kinematic::calDeg_threeSer_new(this->x,this->y,this->z,&deg1,&deg2,&deg3);
   // double newA1 = deg2-old
    
}
Point* Point::tibiaRotate(double angle)
{
    double newX;
    double newY;
    double newZ;
    double originalCoxaAngle = atan2(abs(this->x),abs(this->y))*180/PI;
    double legLength = sqrt( pow(this->x,2.0) + pow(this->y,2.0) );
    cout << "legLength :"<< legLength << endl; 
    double HF = sqrt( pow(legLength-Coxa_Length,2.0) + pow(this->z,2.0) );
    cout << "HF: " << HF << endl;
    double A1 = atan2( (legLength-Coxa_Length),abs(this->z) ) * 180/PI;
    cout << "A1: " << A1 << endl;
    double A2 = acos( ( pow(Tibia_Length,2.0) - pow(Femur_Length,2.0) - pow(HF,2.0) ) / (-2*Femur_Length*HF) ) * 180/PI;
    cout << "A2: " << A2 << endl;
    double femurAngleToYAxis;
  
    femurAngleToYAxis = A1+A2-90;
   
    cout << "femurAngleToYAxis: " << femurAngleToYAxis << endl;
    
    
    double h = Femur_Length*sin(femurAngleToYAxis*PI/180);
    double w = Femur_Length*cos(femurAngleToYAxis*PI/180);
    
    cout << "h: " << h << endl;
    cout << "w: " << w << endl;
    
    if ( this->y > 0 )
    {
        newY = (legLength-Coxa_Length-w)*cos(angle*PI/180.0) - (this->z-h)*sin(angle*PI/180.0) + Coxa_Length+w;
        if ( newY < 0 )
        {
           cout << "Error: Y pos over the limit, invalid pos" << endl;
           return this;
        }
        newZ = (legLength-Coxa_Length-w)*sin(angle*PI/180.0) + (this->z-h)*cos(angle*PI/180.0) + h;
    }
    else if ( this->y < 0 )
    {
        newY = (-1*legLength+Coxa_Length+w)*cos(angle*PI/180.0) - (this->z-h)*sin(angle*PI/180.0) - Coxa_Length-w;
        if ( newY > 0 )
        {
           cout << "Error: Y pos over the limit, invalid pos" << endl;
           return this;
        }
        newZ = (-1*legLength+Coxa_Length+w)*sin(angle*PI/180.0) + (this->z-h)*cos(angle*PI/180.0) + h;
    }
    cout << "before rotate, original angle: " <<  originalCoxaAngle << ", newY:" << newY << endl;
    newX = newY * sin(originalCoxaAngle*PI/180.0);
    if ( this->x >= 0 )
    {
        newX = abs(newX);
    }
    else 
    {
        newX = -1*abs(newX);
    }
    newY = newY * cos(originalCoxaAngle*PI/180.0); // rotate back to global system
    cout << "after rotate, newY: " << newY << endl;
    
    /*
    double D = sqrt(pow(abs(this->y)-Coxa_Length,2.0)+pow(this->z,2.0));
    double angleA = acos( (pow(Femur_Length,2.0)+pow(D,2.0)-pow(Tibia_Length,2.0))/(2*Femur_Length*D));
    double angleB = atan2 (abs(this->z),abs(this->y));
    double angleC;
    if ( this->z >= 0)
    {
        angleC = angleA+angleB;
    }
    else
    {
        angleC = angleA-angleB;
    }
    double h = Femur_Length*sin(angleC);
    double w = Femur_Length*cos(angleC);
    
    if ( this->y > 0 )
    {
        newY = (this->y-Coxa_Length-w)*cos(angle*PI/180.0) - (this->z-h)*sin(angle*PI/180.0) + Coxa_Length+w;
        if ( newY < 0 )
        {
           cout << "Error: Y pos over the limit, invalid pos" << endl;
           return this;
        }
        newZ = (this->y-Coxa_Length-w)*sin(angle*PI/180.0) + (this->z-h)*cos(angle*PI/180.0) + h;
    }
    else if ( this->y < 0 )
    {
        newY = (this->y+Coxa_Length+w)*cos(angle*PI/180.0) - (this->z-h)*sin(angle*PI/180.0) - Coxa_Length-w;
        if ( newY > 0 )
        {
           cout << "Error: Y pos over the limit, invalid pos" << endl;
           return this;
        }
        newZ = (this->y+Coxa_Length+w)*sin(angle*PI/180.0) + (this->z-h)*cos(angle*PI/180.0) + h;
    }
     */
    
    
    //newX = newY/tan(originalCoxaAngle*PI/180.0);
    
    
 
    
    cout << "new Y: " << newY << endl;
    cout << "new Z: " << newZ << endl;
    cout << "new X: " << newX << endl;
    Point * newPoint = new Point (newX, newY, newZ);
    return newPoint;
    
}