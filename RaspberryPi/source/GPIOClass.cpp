#include <fstream>
#include <string>
#include <iostream>
#include <sstream>
#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include "/home/arack/git/hexapod-robot/RaspberryPi/header/GPIOClass.h"

using namespace std;
 
GPIOClass::GPIOClass()
{
    this->gpioNum = "4"; //GPIO4 is default
}
 
GPIOClass::GPIOClass(string gnum , string boardType)
{
    this->gpioNum = gnum;  //Instatiate GPIOClass object for GPIO pin number "gnum"
    this->boardType = boardType;
}
 
int GPIOClass::export_gpio()
{
    string export_str;
    
    if ( boardType == "WRT" )
    {
        export_str = "/sys/class/gpio/export";
    }
    else if ( boardType == "RP")
    {
        export_str = "/sys/class/gpio/export";
    }
    
    cout <<  "file dir is" << export_str << endl;
    int file = open(export_str.c_str(), O_WRONLY);
    if ( file == -1 ){
        cout << "OPERATION FAILED: Unable to open export file "<< this->gpioNum <<"."<< endl;
        return -1;
    }
    //string value = this->gpioNum;
    int writeStatus = write(file, this->gpioNum.c_str(),gpioNum.length());
    if ( writeStatus == -1 )
    {
        cout << "OPERATION FAILED: Unable to write export file "<< this->gpioNum << "." << endl; 
        return -1;
    }
    cout << "Exported " + this->gpioNum << endl;
    int closeStatus = close(file);
    if ( closeStatus == -1 )
    {
        cout << "OPERATION FAILED: Unable to close export file " << this->gpioNum << "." << endl;
        return -1;
    }
    return 0;
}
 
int GPIOClass::unexport_gpio()
{
    string unexport_str;
    if ( this->boardType == "WRT" )
    {
        unexport_str = "/sys/class/gpio/unexport";
    }
    else if ( this->boardType == "RP")
    {
        unexport_str = "/sys/class/gpio/unexport";
    }
    
    int file = open(unexport_str.c_str(), O_WRONLY);
   
    if (file == -1 )
    {
        cout << " OPERATION FAILED: Unable to open unexport file." << endl;
        return -1;
    }
    int writeStatus = write(file, this->gpioNum.c_str(),gpioNum.length());
    if ( writeStatus == -1 )
    {
        cout << " OPERATION FAILED: Unable to write unexport file gpio" << this->gpioNum << "." << endl; 
        return -1;
    }
    cout << "Unexported" + this->gpioNum << endl;
    close(file);
    
    return 0;
}
 

int GPIOClass::setDir(string dir)
{
    string setdir_str;
    if ( this->boardType == "WRT" )
    {
        setdir_str = "/sys/class/gpio/gpio" + this->gpioNum + "/direction";
    }
    else if ( this->boardType == "RP")
    {
        setdir_str = "/sys/class/gpio/gpio" + this->gpioNum + "/direction";
        cout << dir << endl;
    }
     
    cout << "set direction dir" << setdir_str << endl;
    int file = open(setdir_str.c_str(), O_WRONLY);
    if (file == -1)
    {
            cout << " OPERATION FAILED: Unable to open direction file of GPIO"<< this->gpioNum <<"."<< endl;
            return -1;
    }
    
    
    int writeStatus = write(file, dir.c_str(),dir.length());
    if ( writeStatus == -1 )
    {
        cout << " OPERATION FAILED: Unable to write direction file of GPIO"<< this->gpioNum <<"."<< endl;
        return -1; 
    }
    close(file);
    return 0;
}




int GPIOClass::openValFile( char mode )
{
    string setval_str;
    if ( this->boardType == "WRT" )
    {
        setval_str = "/sys/class/gpio/gpio" + this->gpioNum + "/value";
    }
    else if ( this->boardType == "RP")
    {
        setval_str = "/sys/class/gpio/gpio" + this->gpioNum + "/value";
    }
    
    if ( mode == 'w')
    {
        this->valFile = open(setval_str.c_str(), O_WRONLY);
    }
    else if (mode == 'r')
    {
        this->valFile = open(setval_str.c_str(), O_RDONLY);
    }
    else
    {
        cout << "OPERATION FAILED: Error on arg to open value file of GPIO"<< this->gpioNum <<"."<< endl;
        return -1;
    }
    if (this->valFile == -1)
    {
            cout << "OPERATION FAILED: Unable to open the value file of GPIO"<< this->gpioNum <<"."<< endl;
            return -1;
    }
    return 0;
}
 
int GPIOClass::setVal(string val)
{
 
    
    int writeStatus = write(this->valFile, val.c_str(),val.length());
    if ( writeStatus == -1 )
    {
        cout << "OPERATION FAILED: Unable to write the value file of GPIO"<< this->gpioNum <<"."<< endl;
        return -1;
    }
      
    return 0;
}

 
string GPIOClass::getVal(){
 
    
    char value;
    int seek = lseek(this->valFile, 0, SEEK_SET);
    if ( seek == -1 )
    {
            cout << "OPERATION FAILED: cannot seek the value file of GPIO"<< this->gpioNum <<"."<< endl;
            return "-1";
    }
    int readStatus = read(this->valFile, &value, 1);
    if ( readStatus == -1)
    {
            cout << "OPERATION FAILED: cannot read the value file of GPIO"<< this->gpioNum <<"."<< endl;
            return "-1";
    }
    
    
 
    if(value == '0')
    {
        return "0";
    }
    else
    {
        return "1";
    }
 
    return 0;
}

int GPIOClass::closeValFile()
{
    close(this->valFile);
}
 
string GPIOClass::getNum(){
 
return this->gpioNum;
 
}
