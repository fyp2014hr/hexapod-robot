#include <unistd.h>
#include <iostream>
#include <sstream>
#include <fcntl.h>
#include <unistd.h>
#include <math.h>
#include<stdlib.h>
#include<stdio.h>
#include <string> 
#include <map>
#include "/home/arack/git/hexapod-robot/RaspberryPi/header/ServoController.h"
#include "/home/arack/git/hexapod-robot/RaspberryPi/header/CommonResource.h"


using namespace std;

#include <xmlrpc-c/base.h>
#include <xmlrpc-c/client.h> 

#define NAME "Xmlrpc-c Test Client"
#define VERSION "1.0" 
#define MEGEXTRA 1000000


static void dieIfFaultOccurred (xmlrpc_env * const envP) 
{
    if (envP->fault_occurred) 
    {
        fprintf(stderr, "ERROR: %s (%d)\n",
        envP->fault_string, envP->fault_code);
        exit(1);
    }
} 

ServoController::ServoController( string boardType  )
{   
    if ( boardType == "RP" )
    {
        this->dir = "/sys/class/pwm/gpio_pwm:XX/duty_ns";
        this->boardType ="RP";

       
        this->pathMap .insert(pair<int,string>(0,"/sys/class/pwm/gpio_pwm:4/duty_ns"));
        this->pathMap .insert(pair<int,string>(1,"/sys/class/pwm/gpio_pwm:17/duty_ns"));
        this->pathMap .insert(pair<int,string>(2,"/sys/class/pwm/gpio_pwm:18/duty_ns"));
        this->pathMap .insert(pair<int,string>(3,"/sys/class/pwm/gpio_pwm:27/duty_ns"));
        this->pathMap .insert(pair<int,string>(4,"/sys/class/pwm/gpio_pwm:22/duty_ns"));
        this->pathMap .insert(pair<int,string>(5,"/sys/class/pwm/gpio_pwm:23/duty_ns"));
        this->pathMap .insert(pair<int,string>(6,"/sys/class/pwm/gpio_pwm:24/duty_ns"));
        this->pathMap .insert(pair<int,string>(7,"/sys/class/pwm/gpio_pwm:10/duty_ns"));
        this->pathMap .insert(pair<int,string>(8,"/sys/class/pwm/gpio_pwm:9/duty_ns"));
        this->pathMap .insert(pair<int,string>(9,"/sys/class/pwm/gpio_pwm:25/duty_ns"));
        this->pathMap .insert(pair<int,string>(10,"/sys/class/pwm/gpio_pwm:11/duty_ns"));
        this->pathMap .insert(pair<int,string>(11,"/sys/class/pwm/gpio_pwm:8/duty_ns"));
        this->pathMap .insert(pair<int,string>(12,"/sys/class/pwm/gpio_pwm:7/duty_ns"));
        this->pathMap .insert(pair<int,string>(13,"/sys/class/pwm/gpio_pwm:5/duty_ns"));
        this->pathMap .insert(pair<int,string>(14,"/sys/class/pwm/gpio_pwm:6/duty_ns"));
        this->pathMap .insert(pair<int,string>(15,"/sys/class/pwm/gpio_pwm:12/duty_ns"));
        this->pathMap .insert(pair<int,string>(16,"/sys/class/pwm/gpio_pwm:13/duty_ns"));
        this->pathMap .insert(pair<int,string>(17,"/sys/class/pwm/gpio_pwm:19/duty_ns"));
        
        map <int, string>::iterator iter;
        for ( iter = this->pathMap.begin() ; iter != this->pathMap.end() ;iter++ )
        {
            this->fileMap.insert(pair<int,FILE *>(iter->first,fopen(iter->second.c_str(), "w")));
        }
        
        
      
        
        
    }
    
    cout << "going to export file ggggggggggggggggggggggggggg" << endl;
    this->powerSwitch = new GPIOClass( SERVO_POWER_SWITCH_PIN , this->boardType);
    this->powerSwitch->export_gpio();
    this->powerSwitch->setDir("out");
    this->powerSwitch->openValFile('w');
    //this->powerSwitch->setVal("1");
    
    this->powerSwitch2 = new GPIOClass( SERVO_POWER_SWITCH_PIN2 , this->boardType);
    this->powerSwitch2->export_gpio();
    this->powerSwitch2->setDir("out");
    this->powerSwitch2->openValFile('w');
    //this->powerSwitch2->setVal("1");
    
}

int ServoController::powerOn()
{
    
   this->powerSwitch->setVal("1");
   this->powerSwitch2->setVal("1");
   return 0;
}

int ServoController::powerOff()
{
    this->powerSwitch->setVal("0");
    this->powerSwitch2->setVal("0");
    return 0;
 
   /*
   map <int, FILE *>::iterator iter; 
   for ( iter = this->fileMap.begin() ; iter != this->fileMap.end() ;iter++ )
   {
        fprintf(iter->second , "%d",0);
        fseek(iter->second , 0, SEEK_SET);
        fclose(iter->second);
        fflush(iter->second);
   }
   return 0;
    */
}

void ServoController::calibrateDegree ( int index , double * degree )
{
    int offsets[NUM_OF_SERVO] = {10,8,0,4,15,-4,0,7,-2,-5,0,7,7,0,10,8,-3,9};
    int isReverseds[NUM_OF_SERVO] = {ISREVERSED0,ISREVERSED1,ISREVERSED2,ISREVERSED3,ISREVERSED4,ISREVERSED5,ISREVERSED6,ISREVERSED7,ISREVERSED8,ISREVERSED9,ISREVERSED10,ISREVERSED11,ISREVERSED12,ISREVERSED13,ISREVERSED14,ISREVERSED15,ISREVERSED16,ISREVERSED17};
    int offset = offsets[index];
    
   
    bool isReversed = isReverseds[index];
    if ( isReversed )
    {
        *degree -= offset;        
        *degree = 180.0-*degree;
                
    }
    else
    {
                *degree  += offset;
    }
    
}






int ServoController::setPulse(int index, int pulse)
{
  
   fprintf(this->fileMap.at(index) , "%d",pulse);
   fseek(this->fileMap.at(index) , 0, SEEK_SET);
   //fclose(fp);
  // fflush(fp);
   
   return 0;
   
   
}






int ServoController::runOneServo_new(int index, double degree )
{

    double tmp = degree/180.0;
    
       // cout << "tmp=" << tmp << endl;
        double pulse;
        pulse = tmp*2000000.0 + 500000.0;
       // cout << "pulse=" << pulse << endl;
        int roundedPulse = round(pulse);
      //  cout << "roundedPulse=" << roundedPulse << endl;
        
        this->setPulse(index , roundedPulse);
        
   
        
}


int ServoController::runAllServo_new(double degree[])
{
    int offsets[NUM_OF_SERVO] = {-3,8,0,0,15,-2,-6,7,-2,0,-2,3,5,0,8,0,-6,5};
    /*
    cout << "OFFSET:" << endl;
    for ( int i = 0; i< NUM_OF_SERVO ;i++)
    {
            cout << offsets[i] << "  |  ";
    }
    cout <<endl;
    */
    
    if ( this->boardType == "RP" )
    {
       
        double degreeList[NUM_OF_SERVO];
       
        /*
        cout << "degreeList before calib:" << endl;
        for ( int i = 0; i< NUM_OF_SERVO ;i++)
        {
            cout << degree[i] << "  |  ";
        }
        cout <<endl;
        */
        
        
        for ( int i = 0 ; i < NUM_OF_SERVO ; i++ )
        {
            double theDegree = degree[i];
            calibrateDegree(i,&theDegree);
         
            
            
          
                degreeList[i] = theDegree;
          
        }
        /*
        cout << "degreeList after calib:" << endl;
        for ( int i = 0; i< NUM_OF_SERVO ;i++)
        {
            cout << degreeList[i] << "  |  ";
        }
        cout <<endl;
        */
        
        for ( int i = 0; i < NUM_OF_SERVO ; i++ ) 
        {
            runOneServo_new(i,degreeList[i]);
        }
  

    }
}






