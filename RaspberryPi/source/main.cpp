/* 
 * File:   main.cpp
 * Author: arack
 *
 * Created on November 24, 2014, 11:18 PM
 */



#include <cstdlib>
#include <unistd.h>
#include <iostream>
#include <stdio.h>
#include <pthread.h> 
#include <cmath>
#include <sys/types.h>
#include <sys/ipc.h> 
#include <sys/shm.h>
#include <fcntl.h>
#include <bits/basic_string.h>
#include "/home/arack/git/hexapod-robot/RaspberryPi/header/ServoController.h"
#include "/home/arack/git/hexapod-robot/RaspberryPi/header/UltrasoundSensorController.h"
#include "/home/arack/git/hexapod-robot/RaspberryPi/header/XmlRpcServer.h"
#include "/home/arack/git/hexapod-robot/RaspberryPi/header/Kinematic.h"
#include "/home/arack/git/hexapod-robot/RaspberryPi/header/Point.h"
#include "/home/arack/git/hexapod-robot/RaspberryPi/header/Speak.h"
#include "/home/arack/git/hexapod-robot/RaspberryPi/header/PowerClipController.h"



using namespace std;
FILE *fp;

// main component //
XmlRpcServer* xrs;
Kinematic* kine;
ServoController * sc;
UltrasoundSensorController * uc;
Speak * sk;
PowerClipController * pcc;

xmlrpc_env xml_client_env;






// kinematic_para_shmid//
int kine_action_shmId;
int kine_gait_shmId;

int kine_delay_shmId;
int kine_interval_shmId;
int kine_gait_point_delay_shmId;

double kine_posX_shmId;
double kine_posY_shmId;
double kine_posZ_shmId;
double kine_rotX_shmId;
double kine_rotY_shmId;
double kine_rotZ_shmId;

int kine_flag_motionDirty_shmId;




void setShmId()
{
    const key_t ipckey = 24568;
    const int perm = 0666;
    size_t shmSize = 4096;
    kine_action_shmId = shmget(ipckey,shmSize,IPC_CREAT|perm);
    if ( kine_action_shmId == -1 )
    {
        cout << "error get shm" << endl;
    }
    
    
    
    //-----------------------------------------
    
    kine_gait_shmId = shmget(24569,shmSize,IPC_CREAT|perm);
    if ( kine_gait_shmId == -1 )
    {
        cout << "error get gait shm" << endl;
    }
    
    //---------------------------
    kine_delay_shmId = shmget(24570,shmSize,IPC_CREAT|perm);
    if ( kine_delay_shmId == - 1)
    {
        cout << "error get delay shm" << endl;
    }
    
    //-----------
    kine_interval_shmId = shmget(24571,shmSize,IPC_CREAT|perm);
    if ( kine_interval_shmId == - 1)
    {
        cout << "error get interval shm" << endl;
    }
    
    //-----------
    kine_gait_point_delay_shmId = shmget(24572,shmSize,IPC_CREAT|perm);
    if ( kine_gait_point_delay_shmId == - 1)
    {
        cout << "error get gait_point_delay shm" << endl;
    }
    
    //-----------------
    kine_posX_shmId = shmget(24573,shmSize,IPC_CREAT|perm);
    if ( kine_posX_shmId == - 1)
    {
        cout << "error get posX shm" << endl;
    }
    
    //-----
    kine_posY_shmId = shmget(24574,shmSize,IPC_CREAT|perm);
    if ( kine_posY_shmId == - 1)
    {
        cout << "error get posY shm" << endl;
    }
    
    //-----
    kine_posZ_shmId = shmget(24575,shmSize,IPC_CREAT|perm);
    if ( kine_posZ_shmId == - 1)
    {
        cout << "error get posZ shm" << endl;
    }
    
    //-----
    kine_rotX_shmId = shmget(24576,shmSize,IPC_CREAT|perm);
    if ( kine_rotX_shmId == - 1)
    {
        cout << "error get rotX shm" << endl;
    }
    
    //-----
    kine_rotY_shmId = shmget(24577,shmSize,IPC_CREAT|perm);
    if ( kine_rotY_shmId == - 1)
    {
        cout << "error get rotY shm" << endl;
    }
    
    //-----
    kine_rotZ_shmId = shmget(24578,shmSize,IPC_CREAT|perm);
    if ( kine_rotZ_shmId == - 1)
    {
        cout << "error get rotZ shm" << endl;
    }
    
    kine_flag_motionDirty_shmId = shmget(24579,shmSize,IPC_CREAT|perm);
    if ( kine_flag_motionDirty_shmId == -1 )
    {
        cout << "error get kine_flag_motionDirty_shmId"  << endl;
    }
    
    
    
    
    
}







void init_component()
{
  
    sc = new ServoController ("RP");
    kine = new Kinematic( 10000,250000,sc );
    uc = new UltrasoundSensorController ( TRIG_PIN , ECHO_PIN , "RP");
    uc->turnOn();
    sk = new Speak();
    
    pcc = new PowerClipController(PCC_PWM_LEFT,PCC_PWM_RIGHT);
    
}






void * kine_thread_func ( void * arg )
{
    cout << " In the thread kine" << endl;
   
    while ( true )
    {
     
        
        switch ( *(kine->action) )
        {
            
            case GO_FORWARD:
                kine->move_forward();
                break;
            case GO_BACKWARD:
                kine->move_backward();
                break;
            case GO_LEFT:
                kine->move_left();
                break;
            case GO_RIGHT:
                kine->move_right();
                break;
            case DANCE:      
                kine->dance();
                break;
            case STAND:
                kine->stand();
                break; 
            case ROTATE_LEFT:
                kine->rotate_left();
                break;
            case ROTATE_RIGHT:
                kine->rotate_right();
                break;
            case WHEEL_FORWARD_LEFT:
                kine->wheel_forward_left();
                break;
            case WHEEL_FORWARD_RIGHT:
                kine->wheel_forward_right();
                break;
            case WHEEL_BACKWARD_LEFT:
                kine->wheel_backward_left();
                break;
            case WHEEL_BACKWARD_RIGHT:
                kine->wheel_backward_right();
                break;
            case AUTO:
                kine->auto_mode();
                break;
            case MUSIC_DANCE:
                kine->music_dance();
                break;
            case DEMO:
                kine->demo();
                break;
         
                
               
                
            
              
        }
 
        
    }
    
    pthread_exit(NULL);
    
}





int main(int argc, char** argv) {
    

    cout << "last modified : 20150422, 04:15 PM" << endl;
    
    
    xmlrpc_env env;
            
    setShmId();
    init_component();
    
    
    pthread_t kine_thread;

    long t;
    pthread_create(&kine_thread, NULL,
                         kine_thread_func, (void *)&t);
    
    
    xrs = new XmlRpcServer(7070);
    xrs->start();
    
    
    
    return 0;
}
