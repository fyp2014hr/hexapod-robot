#include <iostream>
#include <sstream>
#include <fcntl.h>
#include <unistd.h>
#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/ipc.h> 
#include <sys/shm.h>
#include "/home/arack/git/hexapod-robot/RaspberryPi/header/XmlRpcServer.h"
#include "/home/arack/git/hexapod-robot/RaspberryPi/header/ServoController.h"
#include "/home/arack/git/hexapod-robot/RaspberryPi/header/Kinematic.h"
//#include "/home/arack/git/hexapod-robot/Linux_Server/header/Speak.h"
#define WIN32_LEAN_AND_MEAN /* required by xmlrpc-c/server_abyss.h */
#include <stdlib.h>
#include <stdio.h>
#ifdef _WIN32
# include <windows.h>
# include <winsock2.h>
#else
# include <unistd.h>
#endif
#include <xmlrpc-c/base.h>
#include <xmlrpc-c/server.h>
#include <xmlrpc-c/server_abyss.h>


#ifdef _WIN32
#define SLEEP(seconds) SleepEx(seconds * 1000, 1);
#else
#define SLEEP(seconds) sleep(seconds);
#endif 

static xmlrpc_value *
servo_power_on(xmlrpc_env * const envP,
xmlrpc_value * const paramArrayP,
void * const serverInfo,
void * const channelInfo) {
    cout << "get servo power on command" << endl;
    sc->powerOn();
    
    cout << "finish powerOn" << endl;
    return xmlrpc_build_value(envP, "i", 0);
}


static xmlrpc_value *
servo_power_off(xmlrpc_env * const envP,
xmlrpc_value * const paramArrayP,
void * const serverInfo,
void * const channelInfo) {
    cout << "get servo power off command" << endl;
    sc->powerOff();
    
    cout << "finish powerOff" << endl;
    return xmlrpc_build_value(envP, "i", 0);
}

static xmlrpc_value *
kine_set_gait_point_delay(xmlrpc_env * const envP,
xmlrpc_value * const paramArrayP,
void * const serverInfo,
void * const channelInfo) {
    int gait_point_delay;
/* Parse our argument array. */
    xmlrpc_decompose_value(envP, paramArrayP, "(i)", &gait_point_delay);
    if (envP->fault_occurred)
        return NULL;
    
    int * pointer = (int *) shmat(kine_gait_point_delay_shmId,NULL,0);
    
    *pointer = gait_point_delay;
     
    return xmlrpc_build_value(envP, "i", 0);
    
} 

static xmlrpc_value *
kine_set_interval(xmlrpc_env * const envP,
xmlrpc_value * const paramArrayP,
void * const serverInfo,
void * const channelInfo) {
    int interval;
/* Parse our argument array. */
    xmlrpc_decompose_value(envP, paramArrayP, "(i)", &interval);
    if (envP->fault_occurred)
        return NULL;
    
    int * pointer = (int *) shmat(kine_interval_shmId,NULL,0);
    
    *pointer = interval;
    
    return xmlrpc_build_value(envP, "i", 0);
    
} 

static xmlrpc_value *
kine_set_delay(xmlrpc_env * const envP,
xmlrpc_value * const paramArrayP,
void * const serverInfo,
void * const channelInfo) {
    int delay;
/* Parse our argument array. */
    xmlrpc_decompose_value(envP, paramArrayP, "(i)", &delay);
    if (envP->fault_occurred)
        return NULL;
    
    int * pointer = (int *) shmat(kine_delay_shmId,NULL,0);
    
    *pointer = delay;
    
    return xmlrpc_build_value(envP, "i", 0);
    
} 

static xmlrpc_value *
kine_set_gait(xmlrpc_env * const envP,
xmlrpc_value * const paramArrayP,
void * const serverInfo,
void * const channelInfo) {
    int gait;
/* Parse our argument array. */
    xmlrpc_decompose_value(envP, paramArrayP, "(i)", &gait);
    if (envP->fault_occurred)
        return NULL;
    
    int * pointer = (int *) shmat(kine_gait_shmId,NULL,0);
    
    *pointer = gait;
    
    return xmlrpc_build_value(envP, "i", 0);
    
} 



static xmlrpc_value *
kine_set_action(xmlrpc_env * const envP,
xmlrpc_value * const paramArrayP,
void * const serverInfo,
void * const channelInfo) {
    int action;
/* Parse our argument array. */
    xmlrpc_decompose_value(envP, paramArrayP, "(i)", &action);
    if (envP->fault_occurred)
        return NULL;
    
    int * pointer = (int *) shmat(kine_action_shmId,NULL,0);
    
    *pointer = action;
    
    return xmlrpc_build_value(envP, "i", 0);
    
} 

static xmlrpc_value *
kine_set_posX(xmlrpc_env * const envP,
xmlrpc_value * const paramArrayP,
void * const serverInfo,
void * const channelInfo) {
    double extend;
/* Parse our argument array. */
    xmlrpc_decompose_value(envP, paramArrayP, "(d)", &extend);
    if (envP->fault_occurred)
        return NULL;
   
    double * pointer = (double *) shmat(kine_posX_shmId,NULL,0);
    *pointer = extend;
    
    int * motionDirty = (int *) shmat(kine_flag_motionDirty_shmId,NULL,0);
    *motionDirty = 1;
    
    return xmlrpc_build_value(envP, "i", 0);
    
} 

static xmlrpc_value *
kine_set_posY(xmlrpc_env * const envP,
xmlrpc_value * const paramArrayP,
void * const serverInfo,
void * const channelInfo) {
    double extend;
/* Parse our argument array. */
    xmlrpc_decompose_value(envP, paramArrayP, "(d)", &extend);
    if (envP->fault_occurred)
        return NULL;
   
    double * pointer = (double *) shmat(kine_posY_shmId,NULL,0);
    *pointer = extend;
    
    int * motionDirty = (int *) shmat(kine_flag_motionDirty_shmId,NULL,0);
    *motionDirty = 1;
    
    return xmlrpc_build_value(envP, "i", 0);
    
}

static xmlrpc_value *
kine_set_posZ(xmlrpc_env * const envP,
xmlrpc_value * const paramArrayP,
void * const serverInfo,
void * const channelInfo) {
    double extend;
/* Parse our argument array. */
    xmlrpc_decompose_value(envP, paramArrayP, "(d)", &extend);
    if (envP->fault_occurred)
        return NULL;
   
    double * pointer = (double *) shmat(kine_posZ_shmId,NULL,0);
    *pointer = extend;
    
    int * motionDirty = (int *) shmat(kine_flag_motionDirty_shmId,NULL,0);
    *motionDirty = 1;
    
   
    
    
    return xmlrpc_build_value(envP, "i", 0);
    
}

static xmlrpc_value *
kine_set_rotX(xmlrpc_env * const envP,
xmlrpc_value * const paramArrayP,
void * const serverInfo,
void * const channelInfo) {
    double extend;
/* Parse our argument array. */
    xmlrpc_decompose_value(envP, paramArrayP, "(d)", &extend);
    if (envP->fault_occurred)
        return NULL;
   
    double * pointer = (double *) shmat(kine_rotX_shmId,NULL,0);
    *pointer = extend;
    
    int * motionDirty = (int *) shmat(kine_flag_motionDirty_shmId,NULL,0);
    *motionDirty = 1;
    
    return xmlrpc_build_value(envP, "i", 0);
    
}

static xmlrpc_value *
kine_set_rotY(xmlrpc_env * const envP,
xmlrpc_value * const paramArrayP,
void * const serverInfo,
void * const channelInfo) {
    double extend;
/* Parse our argument array. */
    xmlrpc_decompose_value(envP, paramArrayP, "(d)", &extend);
    if (envP->fault_occurred)
        return NULL;
   
    double * pointer = (double *) shmat(kine_rotY_shmId,NULL,0);
    *pointer = extend;
    
    int * motionDirty = (int *) shmat(kine_flag_motionDirty_shmId,NULL,0);
    *motionDirty = 1;
    
    return xmlrpc_build_value(envP, "i", 0);
    
}

static xmlrpc_value *
kine_set_rotZ(xmlrpc_env * const envP,
xmlrpc_value * const paramArrayP,
void * const serverInfo,
void * const channelInfo) {
    double extend;
/* Parse our argument array. */
    xmlrpc_decompose_value(envP, paramArrayP, "(d)", &extend);
    if (envP->fault_occurred)
        return NULL;
   
    double * pointer = (double *) shmat(kine_rotZ_shmId,NULL,0);
    *pointer = extend;
    
    int * motionDirty = (int *) shmat(kine_flag_motionDirty_shmId,NULL,0);
    *motionDirty = 1;
    
    return xmlrpc_build_value(envP, "i", 0);
    
}

static xmlrpc_value *
servo_run(xmlrpc_env * const envP,
xmlrpc_value * const paramArrayP,
void * const serverInfo,
void * const channelInfo) 
{
    int serNum;
    double degree;
    cout << "get a request13" << endl;
/* Parse our argument array. */
//xmlrpc_decompose_value(envP, paramArrayP, "s", &pin );
    xmlrpc_decompose_value(envP, paramArrayP, "(id)",&serNum,&degree);
    if (envP->fault_occurred)
        return NULL;
    
    
        sc->runOneServo_new(serNum,degree);
   
 

/* Return our result. */
    return xmlrpc_build_value(envP, "i", 0);
} 

static xmlrpc_value *
leg_move(xmlrpc_env * const envP,
xmlrpc_value * const paramArrayP,
void * const serverInfo,
void * const channelInfo) 
{
    int legNum;
    double x,y,z;
    cout << "get a request leg move" << endl;

    xmlrpc_decompose_value(envP, paramArrayP, "(iddd)",&legNum,&x,&y,&z);
    if (envP->fault_occurred)
    return NULL;


    double degree[3];

    kine->calDeg_threeSer_new(x, y, z, &degree[0], &degree[1], &degree[2]);



    xmlrpc_int32 result = 0;
    
    sc->calibrateDegree(legNum*3, &degree[0]);
    sc->calibrateDegree(legNum*3+1, &degree[1]);
    sc->calibrateDegree(legNum*3+2, &degree[2]);
        
    
        sc->runOneServo_new(legNum*3,degree[0]);
        sc->runOneServo_new(legNum*3+1,degree[1]);
        sc->runOneServo_new(legNum*3+2,degree[2]);
    
    
        
    
    

    return xmlrpc_build_value(envP, "i", result);
} 





static xmlrpc_value *
speak(xmlrpc_env * const envP,
xmlrpc_value * const paramArrayP,
void * const serverInfo,
void * const channelInfo) 
{
    char* s;
    cout << "get a request speak" << endl;
    
    //xmlrpc_read_string(envP, paramArrayP, &s);
    xmlrpc_decompose_value(envP, paramArrayP, "(s)",&s);
    if (envP->fault_occurred)
        return NULL;

    string _s = s;
    cout << "string to speak: " << _s << endl;
    sk->speakOnce(_s);
    
    return xmlrpc_build_value(envP, "i", 0);
    
}

static xmlrpc_value *
ultrasound_sensor_read(xmlrpc_env * const envP,
xmlrpc_value * const paramArrayP,
void * const serverInfo,
void * const channelInfo) 
{
    
    cout << "get a request ultrasound_read" << endl;

  
    if (envP->fault_occurred)
        return NULL;


    double value = uc->readOnce();
    
    return xmlrpc_build_value(envP, "d", value);


    
} 

static xmlrpc_value *
    pcc_tight(xmlrpc_env * const envP,
xmlrpc_value * const paramArrayP,
void * const serverInfo,
void * const channelInfo) 
{
    
    cout << "get a request pcc tight" << endl;

  
    if (envP->fault_occurred)
        return NULL;

    pcc->tight();
    
    return xmlrpc_build_value(envP, "i", 0);


    
}

static xmlrpc_value *
    pcc_release(xmlrpc_env * const envP,
xmlrpc_value * const paramArrayP,
void * const serverInfo,
void * const channelInfo) 
{
    
    cout << "get a request pcc tight" << endl;

  
    if (envP->fault_occurred)
        return NULL;

    pcc->release();
    
    return xmlrpc_build_value(envP, "i", 0);


    
} 



using namespace std;

XmlRpcServer::XmlRpcServer( int port )
{
    this->port = port;
    this->isRunning = false;
}



int XmlRpcServer::start()
{

    
    struct xmlrpc_method_info3 const servo_run_method = {
/* .methodName = */ "servo_run",
/* .methodFunction = */ &servo_run,};
    
    struct xmlrpc_method_info3 const leg_move_method = {
/* .methodName = */ "leg_move",
/* .methodFunction = */ &leg_move,};
    
    struct xmlrpc_method_info3 const speak_method = {
/* .methodName = */ "speak",
/* .methodFunction = */ &speak,};
    
    struct xmlrpc_method_info3 const kine_set_action_method = { 
        .methodName =  "kine_set_action",
        .methodFunction =  &kine_set_action};
    
    struct xmlrpc_method_info3 const kine_set_delay_method = { 
        .methodName =  "kine_set_delay",
        .methodFunction =  &kine_set_delay};
    
    struct xmlrpc_method_info3 const kine_set_interval_method = { 
        .methodName =  "kine_set_interval",
        .methodFunction =  &kine_set_interval};
    
    struct xmlrpc_method_info3 const kine_set_gait_point_delay_method = { 
        .methodName =  "kine_set_gait_point_delay",
        .methodFunction =  &kine_set_gait_point_delay};
    
    struct xmlrpc_method_info3 const kine_set_posX_method = { 
        .methodName =  "kine_set_posX",
        .methodFunction =  &kine_set_posX};
    struct xmlrpc_method_info3 const kine_set_posY_method = { 
        .methodName =  "kine_set_posY",
        .methodFunction =  &kine_set_posY};
    struct xmlrpc_method_info3 const kine_set_posZ_method = { 
        .methodName =  "kine_set_posZ",
        .methodFunction =  &kine_set_posZ};
    struct xmlrpc_method_info3 const kine_set_rotX_method = { 
        .methodName =  "kine_set_rotX",
        .methodFunction =  &kine_set_rotX};
    struct xmlrpc_method_info3 const kine_set_rotY_method = { 
        .methodName =  "kine_set_rotY",
        .methodFunction =  &kine_set_rotY};
    struct xmlrpc_method_info3 const kine_set_rotZ_method = { 
        .methodName =  "kine_set_rotZ",
        .methodFunction =  &kine_set_rotZ};
    
    struct xmlrpc_method_info3 const servo_power_on_method = { 
        .methodName =  "servo_power_on",
        .methodFunction =  &servo_power_on};  
    
    struct xmlrpc_method_info3 const servo_power_off_method = { 
        .methodName =  "servo_power_off",
        .methodFunction =  &servo_power_off};
    
    
    struct xmlrpc_method_info3 const ultrasound_sensor_read_method = { 
        .methodName =  "ultrasound_sensor_read",
        .methodFunction =  &ultrasound_sensor_read};
    
    struct xmlrpc_method_info3 const pcc_tight_method = { 
        .methodName =  "pcc_tight",
        .methodFunction =  &pcc_tight};
    
    struct xmlrpc_method_info3 const pcc_release_method = { 
        .methodName =  "pcc_release",
        .methodFunction =  &pcc_release};


    
    
    xmlrpc_server_abyss_parms serverparm;
    xmlrpc_registry * registryP;
    xmlrpc_env env;
    
    xmlrpc_env_init(&env);
    registryP = xmlrpc_registry_new(&env);
    if (env.fault_occurred) {
        printf("xmlrpc_registry_new() failed. %s\n", env.fault_string);
    exit(1);
    }

    xmlrpc_registry_add_method3(&env, registryP, &servo_run_method);
    xmlrpc_registry_add_method3(&env, registryP, &leg_move_method);
    xmlrpc_registry_add_method3(&env, registryP, &speak_method);
    xmlrpc_registry_add_method3(&env, registryP, &kine_set_action_method);
    xmlrpc_registry_add_method3(&env, registryP, &kine_set_delay_method);
    xmlrpc_registry_add_method3(&env, registryP, &kine_set_interval_method);
    xmlrpc_registry_add_method3(&env, registryP, &kine_set_gait_point_delay_method);
    xmlrpc_registry_add_method3(&env, registryP, &kine_set_posX_method);
    xmlrpc_registry_add_method3(&env, registryP, &kine_set_posY_method);
    xmlrpc_registry_add_method3(&env, registryP, &kine_set_posZ_method);
    xmlrpc_registry_add_method3(&env, registryP, &kine_set_rotX_method);
    xmlrpc_registry_add_method3(&env, registryP, &kine_set_rotY_method);
    xmlrpc_registry_add_method3(&env, registryP, &kine_set_rotZ_method);
    xmlrpc_registry_add_method3(&env, registryP, &servo_power_off_method);
    xmlrpc_registry_add_method3(&env, registryP, &servo_power_on_method);
    xmlrpc_registry_add_method3(&env, registryP, &ultrasound_sensor_read_method);
    xmlrpc_registry_add_method3(&env, registryP, &pcc_tight_method);
    xmlrpc_registry_add_method3(&env, registryP, &pcc_release_method);
    if (env.fault_occurred) {
        printf("xmlrpc_registry_add_method3() failed. %s\n",
        env.fault_string);
        exit(1);
    }
    serverparm.config_file_name = NULL; /* Select the modern normal API */
    serverparm.registryP = registryP;
    serverparm.port_number = port;
    serverparm.log_file_name = "/tmp/xmlrpc_log";
    this->isRunning = true;
    printf("Running XML-RPC server...\n");
    xmlrpc_server_abyss(&env, &serverparm, XMLRPC_APSIZE(log_file_name));
    if (env.fault_occurred) {
        printf("xmlrpc_server_abyss() failed. %s\n", env.fault_string);
        exit(1);
    }
    
    
    
/* xmlrpc_server_abyss() never returns unless it fails */
    return 0;
}

int XmlRpcServer::stop()
{
    this->isRunning = false;
}