#include <string>
#include <stdio.h>
#include <unistd.h>
#include "/home/arack/git/hexapod-robot/Linux_Server/header/PowerClipController.h"

using namespace std;
 
PowerClipController::PowerClipController( string pwmLeft, string pwmRight )
{
    this->pwmLeft = pwmLeft;
    this->pwmRight = pwmRight;
}

void PowerClipController::tight()
{
    
    
    string stringLeft = "/sys/class/pwm/gpio_pwm:XX/duty_ns";
    
    stringLeft.replace(24,2,this->pwmLeft);
    
    string stringRight = "/sys/class/pwm/gpio_pwm:XX/duty_ns";
    
    stringRight.replace(24,2,this->pwmRight);
    
    FILE * fileLeft = fopen(stringLeft.c_str(),"w");
    FILE * fileRight = fopen(stringRight.c_str(),"w");
        
    fprintf(fileLeft,"1300000");
    fseek( fileLeft, 0, SEEK_SET);
    
    fprintf(fileRight,"1750000");
    fseek( fileRight, 0, SEEK_SET);
    
    /*
    usleep(500000);
    fprintf(fileLeft,"0");
    fseek( fileLeft, 0, SEEK_SET);
    
    fprintf(fileRight,"0");
    fseek( fileRight, 0, SEEK_SET);
     */
  
        
}

void PowerClipController::release()
{
    
    
    string stringLeft = "/sys/class/pwm/gpio_pwm:XX/duty_ns";
    
    stringLeft.replace(24,2,this->pwmLeft);
    
    string stringRight = "/sys/class/pwm/gpio_pwm:XX/duty_ns";
    
    stringRight.replace(24,2,this->pwmRight);
    
    FILE * fileLeft = fopen(stringLeft.c_str(),"w");
    FILE * fileRight = fopen(stringRight.c_str(),"w");
        
    fprintf(fileLeft,"1600000");
    fseek( fileLeft, 0, SEEK_SET);
    
    fprintf(fileRight,"1400000");
    fseek( fileRight, 0, SEEK_SET);
    
    usleep(100000);
    fprintf(fileLeft,"0");
    fseek( fileLeft, 0, SEEK_SET);
    
    fprintf(fileRight,"0");
    fseek( fileRight, 0, SEEK_SET);
     
  
        
}
