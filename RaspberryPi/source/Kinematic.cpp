#include <iostream>
#include <cmath>
#include <cstdlib>
#include <unistd.h>
#include <stdio.h>
#include <cstring>
#include <sys/types.h>
#include <sys/ipc.h> 
#include <sys/shm.h>
#include "/home/arack/git/hexapod-robot/RaspberryPi/header/Kinematic.h"
#include "/home/arack/git/hexapod-robot/RaspberryPi/header/ServoController.h"
#include "/home/arack/git/hexapod-robot/RaspberryPi/header/Point.h"
#include "/home/arack/git/hexapod-robot/RaspberryPi/header/UltrasoundSensorController.h"

#define MEGEXTRA 1000000
using namespace std;

pthread_attr_t attr;


Kinematic::Kinematic( int delay, int interval, ServoController * sc )
{
    void *pointer = (void *) shmat(kine_action_shmId,NULL,0);
    
    this->action = (int *) pointer;
    *(this->action) = IDLE;
    
    pointer = (void *) shmat(kine_delay_shmId,NULL,0);
    this->delay = (int *) pointer;
    *(this->delay) = delay;
    
    pointer = (void *) shmat(kine_interval_shmId,NULL,0);
    this->interval = (int*) pointer;
    *(this->interval) = interval;
    
    pointer = (void *) shmat(kine_gait_point_delay_shmId,NULL,0);
    this->gait_point_delay = (int*) pointer;
    *(this->gait_point_delay) = 0;
    
    pointer = (void *) shmat(kine_posX_shmId,NULL,0);
    this->posX = (double*) pointer;
    *(this->posX) = 0;
    
    pointer = (void *) shmat(kine_posY_shmId,NULL,0);
    this->posY = (double*) pointer;
    *(this->posY) = 0;
    
    pointer = (void *) shmat(kine_posZ_shmId,NULL,0);
    this->posZ = (double*) pointer;
    *(this->posZ) = 0;
    
    pointer = (void *) shmat(kine_rotX_shmId,NULL,0);
    this->rotX = (double*) pointer;
    *(this->rotX) = 0;
    
    pointer = (void *) shmat(kine_rotY_shmId,NULL,0);
    this->rotY = (double*) pointer;
    *(this->rotY) = 0;
    
    pointer = (void *) shmat(kine_rotZ_shmId,NULL,0);
    this->rotZ = (double*) pointer;
    *(this->rotZ) = 0;
    
    
    pointer = (void *) shmat(kine_gait_shmId,NULL,0);
    this->kine_gait = (int*) pointer;
    *(this->kine_gait) = TRIPOD;
    
    pointer = (void *) shmat(kine_flag_motionDirty_shmId,NULL,0);
    this->motionDirty = (int*) pointer;
    *(this->motionDirty) = 0;
    
    this->natural_gait_right = new Point ( 0 , -1.0*(Coxa_Length+Femur_Length) , -1.0*Tibia_Length);
    this->natural_gait_left = new Point ( 0 , Coxa_Length+Femur_Length , -1.0*Tibia_Length);
    
    *(this->posZ) = -30;
    
    this->natural_gait_left->translateZ( *(this->posZ)  );
    this->natural_gait_right->translateZ( *(this->posZ) );
    
    this->sc = sc;
    this->cal_gait_coordinate();
    this->init_bodyIK_data();
    
    this->state = IDLE;
    
    

}
void Kinematic::init_bodyIK_data()
{
    Point * leg1 = new Point ( InitialPosXLeg0, InitialPosYLeg0, InitialPosZLeg0 );
    Point * leg2 = new Point ( InitialPosXLeg1, InitialPosYLeg1, InitialPosZLeg1 );
    Point * leg3 = new Point ( InitialPosXLeg2, InitialPosYLeg2, InitialPosZLeg2 );
    Point * leg4 = new Point ( InitialPosXLeg3, InitialPosYLeg3, InitialPosZLeg3 );
    Point * leg5 = new Point ( InitialPosXLeg4, InitialPosYLeg4, InitialPosZLeg4 );
    Point * leg6 = new Point ( InitialPosXLeg5, InitialPosYLeg5, InitialPosZLeg5 );
    Point * pointList[NUM_OF_LEG] = { leg1,leg2,leg3,leg4,leg5,leg6 };
    memcpy (this->init_leg_points, pointList, sizeof (pointList));
    
    double offsetX[NUM_OF_LEG] = {BodyCoxaOffsetX_leg0,BodyCoxaOffsetX_leg1,BodyCoxaOffsetX_leg2,BodyCoxaOffsetX_leg3,BodyCoxaOffsetX_leg4,BodyCoxaOffsetX_leg5};
    memcpy (this->coxa_body_center_offsetX, offsetX, sizeof (offsetX)); 
    
    double offsetY[NUM_OF_LEG] = {BodyCoxaOffsetY_leg0,BodyCoxaOffsetY_leg1,BodyCoxaOffsetY_leg2,BodyCoxaOffsetY_leg3,BodyCoxaOffsetY_leg4,BodyCoxaOffsetY_leg5};
    memcpy (this->coxa_body_center_offsetY, offsetY, sizeof (offsetY)); 
}

void Kinematic::cal_gait_coordinate()
{
    
    //tripod: standing coordinate
	Point * pointLeg0_D = this->natural_gait_right->coxaRotate(30.0); 
	Point * pointLeg1_D = this->natural_gait_right;
	Point * pointLeg2_D = this->natural_gait_right->coxaRotate(-30.0);
	Point * pointLeg3_D = this->natural_gait_left->coxaRotate(-30.0);
	Point * pointLeg4_D = this->natural_gait_left;
	Point * pointLeg5_D = this->natural_gait_left->coxaRotate(30.0);
	
	//tripod: forward+backward coordinate(initialize from standing position)
	Point * pointLeg0_A = pointLeg0_D->coxaRotate(-20.0); 
	Point * pointLeg0_B = pointLeg0_D->femurRotate(-40.0); 
        Point * pointLeg0_C = pointLeg0_D->coxaRotate(20.0);
    
        Point * pointLeg1_A = pointLeg1_D->coxaRotate(-20.0);
	Point * pointLeg1_B = pointLeg1_D->femurRotate(-40.0); 
        Point * pointLeg1_C = pointLeg1_D->coxaRotate(20.0);
	
	Point * pointLeg2_A = pointLeg2_D->coxaRotate(-20.0);
	Point * pointLeg2_B = pointLeg2_D->femurRotate(-40.0); 
        Point * pointLeg2_C = pointLeg2_D->coxaRotate(20.0);
	
	Point * pointLeg3_A = pointLeg3_D->coxaRotate(20.0); 
	Point * pointLeg3_B = pointLeg3_D->femurRotate(40.0); 
        Point * pointLeg3_C = pointLeg3_D->coxaRotate(-20.0);
	
	Point * pointLeg4_A = pointLeg4_D->coxaRotate(20.0); 
	Point * pointLeg4_B = pointLeg4_D->femurRotate(40.0); 
        Point * pointLeg4_C = pointLeg4_D->coxaRotate(-20.0);
	
	Point * pointLeg5_A = pointLeg5_D->coxaRotate(20.0); 
	Point * pointLeg5_B = pointLeg5_D->femurRotate(40.0); 
        Point * pointLeg5_C = pointLeg5_D->coxaRotate(-20.0);
	
	//tripod: move forward
	//Phase 1: A->B->C->D->A (leg0,leg2,leg4)
	//Phase 2: C->D->A->B->C (leg1,leg3,leg5)
	Point * listOfStep0_forward[6] = { pointLeg0_A, pointLeg1_C, pointLeg2_A, pointLeg3_C, pointLeg4_A, pointLeg5_C};
	Point * listOfStep1_forward[6] = { pointLeg0_B, pointLeg1_D, pointLeg2_B, pointLeg3_D, pointLeg4_B, pointLeg5_D};
	Point * listOfStep2_forward[6] = { pointLeg0_C, pointLeg1_A, pointLeg2_C, pointLeg3_A, pointLeg4_C, pointLeg5_A};
	Point * listOfStep3_forward[6] = { pointLeg0_D, pointLeg1_B, pointLeg2_D, pointLeg3_B, pointLeg4_D, pointLeg5_B};
    
	memcpy (this->listOfPoints_tripod_forward[0], listOfStep0_forward, sizeof (listOfStep0_forward));
	memcpy (this->listOfPoints_tripod_forward[1], listOfStep1_forward, sizeof (listOfStep1_forward));
	memcpy (this->listOfPoints_tripod_forward[2], listOfStep2_forward, sizeof (listOfStep2_forward));
	memcpy (this->listOfPoints_tripod_forward[3], listOfStep3_forward, sizeof (listOfStep3_forward));
	
	//(step0, step1, step2, step3, step4, step5)
	bool midPointFlag_forward[4] = {false,true,false,true}; //indicate step[x] to step[x+1] is middle point or not
	memcpy (this->midPointStep_tripod_forward, midPointFlag_forward, sizeof (midPointFlag_forward));    
	
	//tripod: move backward
	//Phase 1: C->B->A->D->C (leg0,leg2,leg4)
	//Phase 2: A->D->C->B->A (leg1,leg3,leg5)
	Point * listOfStep0_backward[6] = { pointLeg0_C, pointLeg1_A, pointLeg2_C, pointLeg3_A, pointLeg4_C, pointLeg5_A};
	Point * listOfStep1_backward[6] = { pointLeg0_B, pointLeg1_D, pointLeg2_B, pointLeg3_D, pointLeg4_B, pointLeg5_D};
	Point * listOfStep2_backward[6] = { pointLeg0_A, pointLeg1_C, pointLeg2_A, pointLeg3_C, pointLeg4_A, pointLeg5_C};
	Point * listOfStep3_backward[6] = { pointLeg0_D, pointLeg1_B, pointLeg2_D, pointLeg3_B, pointLeg4_D, pointLeg5_B};
    
	memcpy (this->listOfPoints_tripod_backward[0], listOfStep0_backward, sizeof (listOfStep0_backward));
	memcpy (this->listOfPoints_tripod_backward[1], listOfStep1_backward, sizeof (listOfStep1_backward));
	memcpy (this->listOfPoints_tripod_backward[2], listOfStep2_backward, sizeof (listOfStep2_backward));
	memcpy (this->listOfPoints_tripod_backward[3], listOfStep3_backward, sizeof (listOfStep3_backward));
	
	//(step0, step1, step2, step3, step4, step5)
	bool midPointFlag_backward[4] = {false,true,false,true}; //indicate step[x] to step[x+1] is middle point or not
	memcpy (this->midPointStep_tripod_backward, midPointFlag_backward, sizeof (midPointFlag_backward));
        
        //tripod: crab left/right coordinate
	Point * pointLeg0_crab_D = this->natural_gait_right->coxaRotate(45.0); 
	Point * pointLeg1_crab_D = this->natural_gait_right;
	Point * pointLeg2_crab_D = this->natural_gait_right->coxaRotate(-45.0);
	Point * pointLeg3_crab_D = this->natural_gait_left->coxaRotate(-45.0);
	Point * pointLeg4_crab_D = this->natural_gait_left;
	Point * pointLeg5_crab_D = this->natural_gait_left->coxaRotate(45.0);
	
	Point * pointLeg0_crab_A = pointLeg0_crab_D->coxaRotate(20.0); 
	Point * pointLeg0_crab_B = pointLeg0_crab_D->femurRotate(-40.0); 
        Point * pointLeg0_crab_C = pointLeg0_crab_D->coxaRotate(-20.0);
    
        
        Point * pointLeg1_crab_A = pointLeg1_crab_D->translateY(30.0);
        
	Point * pointLeg1_crab_B = pointLeg1_crab_D->femurRotate(-40.0); 
        
        Point * pointLeg1_crab_C = pointLeg1_crab_D->translateY(-30.0);
       
	
	Point * pointLeg2_crab_A = pointLeg2_crab_D->coxaRotate(-20.0);
	Point * pointLeg2_crab_B = pointLeg2_crab_D->femurRotate(-40.0); 
        Point * pointLeg2_crab_C = pointLeg2_crab_D->coxaRotate(20.0);
	
	Point * pointLeg3_crab_A = pointLeg3_crab_D->coxaRotate(-20.0); 
	Point * pointLeg3_crab_B = pointLeg3_crab_D->femurRotate(40.0); 
        Point * pointLeg3_crab_C = pointLeg3_crab_D->coxaRotate(20.0);
	
	Point * pointLeg4_crab_A = pointLeg4_crab_D->translateY(-30.0);
	Point * pointLeg4_crab_B = pointLeg4_crab_D->femurRotate(40.0); 
        Point * pointLeg4_crab_C = pointLeg4_crab_D->translateY(30.0);
	
	Point * pointLeg5_crab_A = pointLeg5_crab_D->coxaRotate(20.0); 
	Point * pointLeg5_crab_B = pointLeg5_crab_D->femurRotate(40.0); 
        Point * pointLeg5_crab_C = pointLeg5_crab_D->coxaRotate(-20.0);
	
	//tripod: crab move right
	//Right Phase 1: A->B->C->D->A (leg1)
	//Right Phase 2: C->D->A->B->C (leg0,leg2)
	//Left Phase 1: A->D->C->B->A (leg4)
	//Left Phase 2: C->B->A->D->C (leg3,leg5)
	Point * listOfStep1_right[6] = { pointLeg0_crab_C, pointLeg1_crab_A, pointLeg2_crab_C, pointLeg3_crab_C, pointLeg4_crab_A, pointLeg5_crab_C};
	Point * listOfStep2_right[6] = { pointLeg0_crab_D, pointLeg1_crab_B, pointLeg2_crab_D, pointLeg3_crab_B, pointLeg4_crab_D, pointLeg5_crab_B};
	Point * listOfStep3_right[6] = { pointLeg0_crab_A, pointLeg1_crab_C, pointLeg2_crab_A, pointLeg3_crab_A, pointLeg4_crab_C, pointLeg5_crab_A};
	Point * listOfStep4_right[6] = { pointLeg0_crab_B, pointLeg1_crab_D, pointLeg2_crab_B, pointLeg3_crab_D, pointLeg4_crab_B, pointLeg5_crab_D};
	
	memcpy (this->listOfPoints_crab_right[0], listOfStep1_right, sizeof (listOfStep1_right));
	memcpy (this->listOfPoints_crab_right[1], listOfStep2_right, sizeof (listOfStep2_right));
	memcpy (this->listOfPoints_crab_right[2], listOfStep3_right, sizeof (listOfStep3_right));
	memcpy (this->listOfPoints_crab_right[3], listOfStep4_right, sizeof (listOfStep4_right));


	bool midPointTrueFalse_move_right[4] = {false,true,false,true};
	memcpy (this->midPointStep_crab_right, midPointTrueFalse_move_right, sizeof (midPointTrueFalse_move_right));
	
	//tripod: crab move left
	//Left Phase 1: A->B->C->D->A (leg4)
	//Left Phase 2: C->D->A->B->C (leg3,leg5)
	//Right Phase 1: A->D->C->B->A (leg1)
	//Right Phase 2: C->B->A->D->C (leg0,leg2)
	Point * listOfStep1_left[6] = { pointLeg0_crab_C, pointLeg1_crab_A, pointLeg2_crab_C, pointLeg3_crab_C, pointLeg4_crab_A, pointLeg5_crab_C};
	Point * listOfStep2_left[6] = { pointLeg0_crab_B, pointLeg1_crab_D, pointLeg2_crab_B, pointLeg3_crab_D, pointLeg4_crab_B, pointLeg5_crab_D};
	Point * listOfStep3_left[6] = { pointLeg0_crab_A, pointLeg1_crab_C, pointLeg2_crab_A, pointLeg3_crab_A, pointLeg4_crab_C, pointLeg5_crab_A};
	Point * listOfStep4_left[6] = { pointLeg0_crab_D, pointLeg1_crab_B, pointLeg2_crab_D, pointLeg3_crab_B, pointLeg4_crab_D, pointLeg5_crab_B};
	
	memcpy (this->listOfPoints_crab_left[0], listOfStep1_left, sizeof (listOfStep1_left));
	memcpy (this->listOfPoints_crab_left[1], listOfStep2_left, sizeof (listOfStep2_left));
	memcpy (this->listOfPoints_crab_left[2], listOfStep3_left, sizeof (listOfStep3_left));
	memcpy (this->listOfPoints_crab_left[3], listOfStep4_left, sizeof (listOfStep4_left));


	bool midPointTrueFalse_move_left[4] = {false,true,false,true};
	memcpy (this->midPointStep_crab_left, midPointTrueFalse_move_left, sizeof (midPointTrueFalse_move_left));
	
        //rotate right
        Point * listOfStep0Rotate_right[6] = { pointLeg0_C, pointLeg1_A, pointLeg2_C, pointLeg3_C, pointLeg4_A, pointLeg5_C};
	Point * listOfStep1Rotate_right[6] = { pointLeg0_B, pointLeg1_D, pointLeg2_B, pointLeg3_D, pointLeg4_B, pointLeg5_D};
	Point * listOfStep2Rotate_right[6] = { pointLeg0_A, pointLeg1_C, pointLeg2_A, pointLeg3_A, pointLeg4_C, pointLeg5_A};
	Point * listOfStep3Rotate_right[6] = { pointLeg0_D, pointLeg1_B, pointLeg2_D, pointLeg3_B, pointLeg4_D, pointLeg5_B};

	memcpy (this->listOfPoints_rotate_right[0], listOfStep0Rotate_right, sizeof (listOfStep0Rotate_right));
	memcpy (this->listOfPoints_rotate_right[1], listOfStep1Rotate_right, sizeof (listOfStep1Rotate_right));
	memcpy (this->listOfPoints_rotate_right[2], listOfStep2Rotate_right, sizeof (listOfStep2Rotate_right));
	memcpy (this->listOfPoints_rotate_right[3], listOfStep3Rotate_right, sizeof (listOfStep3Rotate_right));
	

	bool midPointFlag_rotate_right[4] = {false,true,false,true};
	memcpy (this->midPointStep_rotate_right, midPointFlag_rotate_right, sizeof (midPointFlag_rotate_right));
        
        	//tripod: rotate left
	//Right Phase 1: A->B->C->D->A (leg1)
	//Right Phase 2: C->D->A->B->C (leg0,leg2)
	//Left Phase 1: A->D->C->B->A (leg4)
	//Left Phase 2: C->B->A->D->C (leg3,leg5)
	Point * listOfStep0Rotate_left[6] = { pointLeg0_C, pointLeg1_A, pointLeg2_C, pointLeg3_C, pointLeg4_A, pointLeg5_C};
	Point * listOfStep1Rotate_left[6] = { pointLeg0_D, pointLeg1_B, pointLeg2_D, pointLeg3_B, pointLeg4_D, pointLeg5_B};
	Point * listOfStep2Rotate_left[6] = { pointLeg0_A, pointLeg1_C, pointLeg2_A, pointLeg3_A, pointLeg4_C, pointLeg5_A};
	Point * listOfStep3Rotate_left[6] = { pointLeg0_B, pointLeg1_D, pointLeg2_B, pointLeg3_D, pointLeg4_B, pointLeg5_D};

	memcpy (this->listOfPoints_rotate_left[0], listOfStep0Rotate_left, sizeof (listOfStep0Rotate_left));
	memcpy (this->listOfPoints_rotate_left[1], listOfStep1Rotate_left, sizeof (listOfStep1Rotate_left));
	memcpy (this->listOfPoints_rotate_left[2], listOfStep2Rotate_left, sizeof (listOfStep2Rotate_left));
	memcpy (this->listOfPoints_rotate_left[3], listOfStep3Rotate_left, sizeof (listOfStep3Rotate_left));
	

	bool midPointFlag_rotate_left[4] = {false,true,false,true};
	memcpy (this->midPointStep_rotate_left, midPointFlag_rotate_left, sizeof (midPointFlag_rotate_left));
        
        
        	//tripod: Right wheel coordinate
	Point * pointLeg0_wheelRight_D = natural_gait_right->coxaRotate(30.0); 
	Point * pointLeg1_wheelRight_D = natural_gait_right;
	Point * pointLeg2_wheelRight_D = natural_gait_right->coxaRotate(-30.0);
	Point * pointLeg3_wheelRight_D = natural_gait_left->coxaRotate(-45.0);
	Point * pointLeg4_wheelRight_D = natural_gait_left;
	Point * pointLeg5_wheelRight_D = natural_gait_left->coxaRotate(45.0);
	
	Point * pointLeg0_wheelRight_A = pointLeg0_wheelRight_D->coxaRotate(-15.0); 
	Point * pointLeg0_wheelRight_B = pointLeg0_wheelRight_D->femurRotate(-40.0); 
        Point * pointLeg0_wheelRight_C = pointLeg0_wheelRight_D->coxaRotate(15.0);
    
        Point * pointLeg1_wheelRight_A = pointLeg1_wheelRight_D->coxaRotate(-15.0);
	Point * pointLeg1_wheelRight_B = pointLeg1_wheelRight_D->femurRotate(-40.0); 
        Point * pointLeg1_wheelRight_C = pointLeg1_wheelRight_D->coxaRotate(15.0);
	
	Point * pointLeg2_wheelRight_A = pointLeg2_wheelRight_D->coxaRotate(-15.0);
	Point * pointLeg2_wheelRight_B = pointLeg2_wheelRight_D->femurRotate(-40.0); 
        Point * pointLeg2_wheelRight_C = pointLeg2_wheelRight_D->coxaRotate(15.0);
	
	Point * pointLeg3_wheelRight_A = pointLeg3_wheelRight_D->coxaRotate(25.0); 
	Point * pointLeg3_wheelRight_B = pointLeg3_wheelRight_D->femurRotate(40.0); 
        Point * pointLeg3_wheelRight_C = pointLeg3_wheelRight_D->coxaRotate(-25.0);
	
	Point * pointLeg4_wheelRight_A = pointLeg4_wheelRight_D->coxaRotate(25.0); 
	Point * pointLeg4_wheelRight_B = pointLeg4_wheelRight_D->femurRotate(40.0); 
        Point * pointLeg4_wheelRight_C = pointLeg4_wheelRight_D->coxaRotate(-25.0);
	
	Point * pointLeg5_wheelRight_A = pointLeg5_wheelRight_D->coxaRotate(25.0); 
	Point * pointLeg5_wheelRight_B = pointLeg5_wheelRight_D->femurRotate(40.0); 
        Point * pointLeg5_wheelRight_C = pointLeg5_wheelRight_D->coxaRotate(-25.0);
	
	//Tripod: forward right wheel
	//Phase 1: A->B->C->D->A (leg0,leg2,leg4)
	//Phase 2: C->D->A->B->C (leg1,leg3,leg5)
	Point * listOfStep0_forwardWheelRight[6] = { pointLeg0_wheelRight_A, pointLeg1_wheelRight_C, pointLeg2_wheelRight_A, pointLeg3_wheelRight_C, pointLeg4_wheelRight_A, pointLeg5_wheelRight_C};
	Point * listOfStep1_forwardWheelRight[6] = { pointLeg0_wheelRight_B, pointLeg1_wheelRight_D, pointLeg2_wheelRight_B, pointLeg3_wheelRight_D, pointLeg4_wheelRight_B, pointLeg5_wheelRight_D};
	Point * listOfStep2_forwardWheelRight[6] = { pointLeg0_wheelRight_C, pointLeg1_wheelRight_A, pointLeg2_wheelRight_C, pointLeg3_wheelRight_A, pointLeg4_wheelRight_C, pointLeg5_wheelRight_A};
	Point * listOfStep3_forwardWheelRight[6] = { pointLeg0_wheelRight_D, pointLeg1_wheelRight_B, pointLeg2_wheelRight_D, pointLeg3_wheelRight_B, pointLeg4_wheelRight_D, pointLeg5_wheelRight_B};
    
	memcpy (this->listOfPoints_tripod_forwardWheelRight[0], listOfStep0_forwardWheelRight, sizeof (listOfStep0_forwardWheelRight));
	memcpy (this->listOfPoints_tripod_forwardWheelRight[1], listOfStep1_forwardWheelRight, sizeof (listOfStep1_forwardWheelRight));
	memcpy (this->listOfPoints_tripod_forwardWheelRight[2], listOfStep2_forwardWheelRight, sizeof (listOfStep2_forwardWheelRight));
	memcpy (this->listOfPoints_tripod_forwardWheelRight[3], listOfStep3_forwardWheelRight, sizeof (listOfStep3_forwardWheelRight));
	
	bool midPointFlag_forwardWheelRight[4] = {false,true,false,true}; //indicate step[x] to step[x+1] is middle point or not
	memcpy (this->midPointStep_tripod_forwardWheelRight, midPointFlag_forwardWheelRight, sizeof (midPointFlag_forwardWheelRight));    
	
	//tripod: backward Right wheel
	//Phase 1: C->B->A->D->C (leg0,leg2,leg4)
	//Phase 2: A->D->C->B->A (leg1,leg3,leg5)
	Point * listOfStep0_backwardWheelRight[6] = { pointLeg0_wheelRight_C, pointLeg1_wheelRight_A, pointLeg2_wheelRight_C, pointLeg3_wheelRight_A, pointLeg4_wheelRight_C, pointLeg5_wheelRight_A};
	Point * listOfStep1_backwardWheelRight[6] = { pointLeg0_wheelRight_B, pointLeg1_wheelRight_D, pointLeg2_wheelRight_B, pointLeg3_wheelRight_D, pointLeg4_wheelRight_B, pointLeg5_wheelRight_D};
	Point * listOfStep2_backwardWheelRight[6] = { pointLeg0_wheelRight_A, pointLeg1_wheelRight_C, pointLeg2_wheelRight_A, pointLeg3_wheelRight_C, pointLeg4_wheelRight_A, pointLeg5_wheelRight_C};
	Point * listOfStep3_backwardWheelRight[6] = { pointLeg0_wheelRight_D, pointLeg1_wheelRight_B, pointLeg2_wheelRight_D, pointLeg3_wheelRight_B, pointLeg4_wheelRight_D, pointLeg5_wheelRight_B};
    
	memcpy (this->listOfPoints_tripod_backwardWheelRight[0], listOfStep0_backwardWheelRight, sizeof (listOfStep0_backwardWheelRight));
	memcpy (this->listOfPoints_tripod_backwardWheelRight[1], listOfStep1_backwardWheelRight, sizeof (listOfStep1_backwardWheelRight));
	memcpy (this->listOfPoints_tripod_backwardWheelRight[2], listOfStep2_backwardWheelRight, sizeof (listOfStep2_backwardWheelRight));
	memcpy (this->listOfPoints_tripod_backwardWheelRight[3], listOfStep3_backwardWheelRight, sizeof (listOfStep3_backwardWheelRight));

	bool midPointFlag_backwardWheelRight[4] = {false,true,false,true}; //indicate step[x] to step[x+1] is middle point or not
	memcpy (this->midPointStep_tripod_backwardWheelRight, midPointFlag_backwardWheelRight, sizeof (midPointFlag_backwardWheelRight));
	
	//tripod: Left wheel coordinate
	Point * pointLeg0_wheelLeft_D = natural_gait_right->coxaRotate(45.0); 
	Point * pointLeg1_wheelLeft_D = natural_gait_right;
	Point * pointLeg2_wheelLeft_D = natural_gait_right->coxaRotate(-45.0);
	Point * pointLeg3_wheelLeft_D = natural_gait_left->coxaRotate(-30.0);
	Point * pointLeg4_wheelLeft_D = natural_gait_left;
	Point * pointLeg5_wheelLeft_D = natural_gait_left->coxaRotate(30.0);
	
	Point * pointLeg0_wheelLeft_A = pointLeg0_wheelLeft_D->coxaRotate(-25.0); 
	Point * pointLeg0_wheelLeft_B = pointLeg0_wheelLeft_D->femurRotate(-40.0); 
        Point * pointLeg0_wheelLeft_C = pointLeg0_wheelLeft_D->coxaRotate(25.0);
    
        Point * pointLeg1_wheelLeft_A = pointLeg1_wheelLeft_D->coxaRotate(-25.0);
	Point * pointLeg1_wheelLeft_B = pointLeg1_wheelLeft_D->femurRotate(-40.0); 
        Point * pointLeg1_wheelLeft_C = pointLeg1_wheelLeft_D->coxaRotate(25.0);
	
	Point * pointLeg2_wheelLeft_A = pointLeg2_wheelLeft_D->coxaRotate(-25.0);
	Point * pointLeg2_wheelLeft_B = pointLeg2_wheelLeft_D->femurRotate(-40.0); 
        Point * pointLeg2_wheelLeft_C = pointLeg2_wheelLeft_D->coxaRotate(25.0);
	
	Point * pointLeg3_wheelLeft_A = pointLeg3_wheelLeft_D->coxaRotate(15.0); 
	Point * pointLeg3_wheelLeft_B = pointLeg3_wheelLeft_D->femurRotate(40.0); 
        Point * pointLeg3_wheelLeft_C = pointLeg3_wheelLeft_D->coxaRotate(-15.0);
	
	Point * pointLeg4_wheelLeft_A = pointLeg4_wheelLeft_D->coxaRotate(15.0); 
	Point * pointLeg4_wheelLeft_B = pointLeg4_wheelLeft_D->femurRotate(40.0); 
        Point * pointLeg4_wheelLeft_C = pointLeg4_wheelLeft_D->coxaRotate(-15.0);
	
	Point * pointLeg5_wheelLeft_A = pointLeg5_wheelLeft_D->coxaRotate(15.0); 
	Point * pointLeg5_wheelLeft_B = pointLeg5_wheelLeft_D->femurRotate(40.0); 
        Point * pointLeg5_wheelLeft_C = pointLeg5_wheelLeft_D->coxaRotate(-15.0);
	
	//Tripod: foward left wheel
	//Phase 1: A->B->C->D->A (leg0,leg2,leg4)
	//Phase 2: C->D->A->B->C (leg1,leg3,leg5)
	Point * listOfStep0_forwardWheelLeft[6] = { pointLeg0_wheelLeft_A, pointLeg1_wheelLeft_C, pointLeg2_wheelLeft_A, pointLeg3_wheelLeft_C, pointLeg4_wheelLeft_A, pointLeg5_wheelLeft_C};
	Point * listOfStep1_forwardWheelLeft[6] = { pointLeg0_wheelLeft_B, pointLeg1_wheelLeft_D, pointLeg2_wheelLeft_B, pointLeg3_wheelLeft_D, pointLeg4_wheelLeft_B, pointLeg5_wheelLeft_D};
	Point * listOfStep2_forwardWheelLeft[6] = { pointLeg0_wheelLeft_C, pointLeg1_wheelLeft_A, pointLeg2_wheelLeft_C, pointLeg3_wheelLeft_A, pointLeg4_wheelLeft_C, pointLeg5_wheelLeft_A};
	Point * listOfStep3_forwardWheelLeft[6] = { pointLeg0_wheelLeft_D, pointLeg1_wheelLeft_B, pointLeg2_wheelLeft_D, pointLeg3_wheelLeft_B, pointLeg4_wheelLeft_D, pointLeg5_wheelLeft_B};
    
	memcpy (this->listOfPoints_tripod_forwardWheelLeft[0], listOfStep0_forwardWheelLeft, sizeof (listOfStep0_forwardWheelLeft));
	memcpy (this->listOfPoints_tripod_forwardWheelLeft[1], listOfStep1_forwardWheelLeft, sizeof (listOfStep1_forwardWheelLeft));
	memcpy (this->listOfPoints_tripod_forwardWheelLeft[2], listOfStep2_forwardWheelLeft, sizeof (listOfStep2_forwardWheelLeft));
	memcpy (this->listOfPoints_tripod_forwardWheelLeft[3], listOfStep3_forwardWheelLeft, sizeof (listOfStep3_forwardWheelLeft));
	
	bool midPointFlag_forwardWheelLeft[4] = {false,true,false,true}; //indicate step[x] to step[x+1] is middle point or not
	memcpy (this->midPointStep_tripod_forwardWheelLeft, midPointFlag_forwardWheelLeft, sizeof (midPointFlag_forwardWheelLeft));

	//tripod: backward left wheel
	//Phase 1: C->B->A->D->C (leg0,leg2,leg4)
	//Phase 2: A->D->C->B->A (leg1,leg3,leg5)
	Point * listOfStep0_backwardWheelLeft[6] = { pointLeg0_wheelLeft_C, pointLeg1_wheelLeft_A, pointLeg2_wheelLeft_C, pointLeg3_wheelLeft_A, pointLeg4_wheelLeft_C, pointLeg5_wheelLeft_A};
	Point * listOfStep1_backwardWheelLeft[6] = { pointLeg0_wheelLeft_B, pointLeg1_wheelLeft_D, pointLeg2_wheelLeft_B, pointLeg3_wheelLeft_D, pointLeg4_wheelLeft_B, pointLeg5_wheelLeft_D};
	Point * listOfStep2_backwardWheelLeft[6] = { pointLeg0_wheelLeft_A, pointLeg1_wheelLeft_C, pointLeg2_wheelLeft_A, pointLeg3_wheelLeft_C, pointLeg4_wheelLeft_A, pointLeg5_wheelLeft_C};
	Point * listOfStep3_backwardWheelLeft[6] = { pointLeg0_wheelLeft_D, pointLeg1_wheelLeft_B, pointLeg2_wheelLeft_D, pointLeg3_wheelLeft_B, pointLeg4_wheelLeft_D, pointLeg5_wheelLeft_B};
    
	memcpy (this->listOfPoints_tripod_backwardWheelLeft[0], listOfStep0_backwardWheelLeft, sizeof (listOfStep0_backwardWheelLeft));
	memcpy (this->listOfPoints_tripod_backwardWheelLeft[1], listOfStep1_backwardWheelLeft, sizeof (listOfStep1_backwardWheelLeft));
	memcpy (this->listOfPoints_tripod_backwardWheelLeft[2], listOfStep2_backwardWheelLeft, sizeof (listOfStep2_backwardWheelLeft));
	memcpy (this->listOfPoints_tripod_backwardWheelLeft[3], listOfStep3_backwardWheelLeft, sizeof (listOfStep3_backwardWheelLeft));

	bool midPointFlag_backwardWheelLeft[4] = {false,true,false,true}; //indicate step[x] to step[x+1] is middle point or not
	memcpy (this->midPointStep_tripod_backwardWheelLeft, midPointFlag_backwardWheelLeft, sizeof (midPointFlag_backwardWheelLeft));
   
}

void Kinematic:: demo()
{
    int originalInterval = *(this->interval);
    *(this->interval) = 400000;
    Point * legsPoint[NUM_OF_LEG];
    double degList[NUM_OF_SERVO];
   
    *(this->posX) = -50;
    
        this->bodyIK(legsPoint);
        this->legsIK(legsPoint,degList);
        this->moveToDegs(degList,*(this->interval));
       
    *(this->posX) = 50;   
        this->bodyIK(legsPoint);
        this->legsIK(legsPoint,degList);
        this->moveToDegs(degList,*(this->interval));
     
     *(this->posX) = 0; 
        this->bodyIK(legsPoint);
        this->legsIK(legsPoint,degList);
        this->moveToDegs(degList,*(this->interval));
     
     usleep (500000);
     
     
     *(this->posY) = -50;
    
        this->bodyIK(legsPoint);
        this->legsIK(legsPoint,degList);
        this->moveToDegs(degList,*(this->interval));
       
    *(this->posY) = 50;   
        this->bodyIK(legsPoint);
        this->legsIK(legsPoint,degList);
        this->moveToDegs(degList,*(this->interval));
     
     *(this->posY) = 0; 
        this->bodyIK(legsPoint);
        this->legsIK(legsPoint,degList);
        this->moveToDegs(degList,*(this->interval));
        
     usleep (500000);
     
     
     *(this->posZ) = 20;
    
        this->bodyIK(legsPoint);
        this->legsIK(legsPoint,degList);
        this->moveToDegs(degList,*(this->interval));
       
    *(this->posZ) = -30;   
        this->bodyIK(legsPoint);
        this->legsIK(legsPoint,degList);
        this->moveToDegs(degList,*(this->interval));
     
    usleep (500000);
     
     
     *(this->rotX) = 15;
    
        this->bodyIK(legsPoint);
        this->legsIK(legsPoint,degList);
        this->moveToDegs(degList,*(this->interval));
       
    *(this->rotX) = -15;   
        this->bodyIK(legsPoint);
        this->legsIK(legsPoint,degList);
        this->moveToDegs(degList,*(this->interval));
     
     *(this->rotX) = 0; 
        this->bodyIK(legsPoint);
        this->legsIK(legsPoint,degList);
        this->moveToDegs(degList,*(this->interval));
     
     usleep (500000);
     
     
     *(this->rotY) = 15;
    
        this->bodyIK(legsPoint);
        this->legsIK(legsPoint,degList);
        this->moveToDegs(degList,*(this->interval));
       
    *(this->rotY) = -15;   
        this->bodyIK(legsPoint);
        this->legsIK(legsPoint,degList);
        this->moveToDegs(degList,*(this->interval));
     
     *(this->rotY) = 0; 
        this->bodyIK(legsPoint);
        this->legsIK(legsPoint,degList);
        this->moveToDegs(degList,*(this->interval));
     
        
     usleep (500000);
     
     
     *(this->rotZ) = 20;
    
        this->bodyIK(legsPoint);
        this->legsIK(legsPoint,degList);
        this->moveToDegs(degList,*(this->interval));
       
    *(this->rotZ) = -20;   
        this->bodyIK(legsPoint);
        this->legsIK(legsPoint,degList);
        this->moveToDegs(degList,*(this->interval));
     
     *(this->rotZ) = 0; 
        this->bodyIK(legsPoint);
        this->legsIK(legsPoint,degList);
        this->moveToDegs(degList,*(this->interval));   
        
      usleep (500000);
      
      *(this->interval) = originalInterval;
       
}
void Kinematic::music_dance()
{
    cout << "start dance " << endl;
    Point * legsPoint[NUM_OF_LEG];
    double degList[NUM_OF_SERVO];
    int originalInterval = *(this->interval);
    *(this->interval) = 150000;
    
    popen( "madplay -s \"00:00:15\" -t \"00:00:16\" /tmp/data/music/My-Boy-SISTAR.mp3.mp3", "r" );
    //sleep 6s
    usleep(6500000);
    
    for(int i=0;i<7;i++){
        
        *(this->posZ) = -20;
        this->bodyIK(legsPoint);
        this->legsIK(legsPoint,this->currentDegList);
        this->sc->runAllServo_new(this->currentDegList);
        
        usleep(200000); //0.2s between movement
        
        *(this->posZ) = 20;
        this->bodyIK(legsPoint);
        this->legsIK(legsPoint,this->currentDegList);
        this->sc->runAllServo_new(this->currentDegList);
        
        usleep(1200000); //1.3s
        //13/7 = 1.8s pershake
    }
    //sleep 1s
    usleep(1000000);
    
    popen( "madplay -s \"00:01:11\" -t \"00:00:25\" /tmp/data/music/My-Boy-SISTAR.mp3.mp3", "r" );
    
    *(this->posZ) = -20;
    this->bodyIK(legsPoint);
    this->legsIK(legsPoint,this->currentDegList);
    this->sc->runAllServo_new(this->currentDegList);
    
    usleep(4500000);
    for(int j=0;j<2;j++)
    {//oh my boy x2
        for (int i = 0; i < 3 ; i++)
        { //3 shake
            *(this->posZ) = 0;
            *(this->posX) = -40;
            this->bodyIK(legsPoint);
            this->legsIK(legsPoint,degList);
            this->moveToDegs(degList,*(this->interval));
            
            *(this->posZ) = 20;
            *(this->posX) = 0;
            this->bodyIK(legsPoint);
            this->legsIK(legsPoint,degList);
            this->moveToDegs(degList,*(this->interval));
            
            
            *(this->posZ) = 0;
            *(this->posX) = 40;
            this->bodyIK(legsPoint);
            this->legsIK(legsPoint,degList);
            this->moveToDegs(degList,*(this->interval));
            
            *(this->posX) = 0;
            *(this->posZ) = -30;
            this->bodyIK(legsPoint);
            this->legsIK(legsPoint,degList);
            this->moveToDegs(degList,*(this->interval));
        }
        usleep(500000);//hand period 0.5s
    }
    usleep(5500000); //wait 5s
    for(int j=0;j<2;j++)
    {//oh my boy x2
        for (int i = 0; i < 3 ; i++)
        { //3 shake
            *(this->posZ) = 0;
            *(this->posX) = -40;
            this->bodyIK(legsPoint);
            this->legsIK(legsPoint,degList);
            this->moveToDegs(degList,*(this->interval));
            
            *(this->posZ) = 20;
            *(this->posX) = 0;
            this->bodyIK(legsPoint);
            this->legsIK(legsPoint,degList);
            this->moveToDegs(degList,*(this->interval));
            
            
            *(this->posZ) = 0;
            *(this->posX) = 40;
            this->bodyIK(legsPoint);
            this->legsIK(legsPoint,degList);
            this->moveToDegs(degList,*(this->interval));
            
            *(this->posX) = 0;
            *(this->posZ) = -30;
            this->bodyIK(legsPoint);
            this->legsIK(legsPoint,degList);
            this->moveToDegs(degList,*(this->interval));
        }
        usleep(500000);//hand
    }
    usleep(10000000);  
    *(this->interval) = originalInterval;
    return;


        

}

void Kinematic::dance()
{
    if (this->state != DANCE )
    {
  
        this->state = DANCE;
        
    }
    int originalInterval = *(this->interval);
   //*(this->interval) = 100000;
    if ( *(this->motionDirty) == 1 )
    {
        *(this->interval) = 300000;
        Point * legsPoint[NUM_OF_LEG];
        double degreeList[NUM_OF_SERVO];
        this->bodyIK(legsPoint);
        this->legsIK(legsPoint,degreeList);
        this->moveToDegs(degreeList,*(this->interval));
        //this->sc->runAllServo_new(degreeList);
        this->natural_gait_left->translateZ( *(this->posZ)  );
        this->natural_gait_right->translateZ( *(this->posZ) );
       
        this->cal_gait_coordinate();
        *(this->motionDirty) = 0;
        *(this->interval) = originalInterval;
    }
   
    usleep(500000);
    
}
void Kinematic::rotate_right()
{
    if ( this->state != ROTATE_RIGHT )
        this->state = ROTATE_RIGHT;
    this->moveAlongPoints_new( this->listOfPoints_rotate_right, this->midPointStep_rotate_right,4);
}
void Kinematic::rotate_left()
{
    if ( this->state != ROTATE_LEFT )
        this->state = ROTATE_LEFT;
    this->moveAlongPoints_new( this->listOfPoints_rotate_left, this->midPointStep_rotate_left,4);    
    
}

void Kinematic::wheel_forward_left()
{
    if ( this->state != WHEEL_FORWARD_LEFT )
        this->state = WHEEL_FORWARD_LEFT;
    this->moveAlongPoints_new( this->listOfPoints_tripod_forwardWheelLeft, this->midPointStep_tripod_forwardWheelLeft,4); 
}
void Kinematic::wheel_forward_right()
{
    if ( this->state != WHEEL_FORWARD_RIGHT )
        this->state = WHEEL_FORWARD_RIGHT;
    this->moveAlongPoints_new( this->listOfPoints_tripod_forwardWheelRight, this->midPointStep_tripod_forwardWheelRight,4); 
}
void Kinematic::wheel_backward_left()
{
    if ( this->state != WHEEL_BACKWARD_LEFT )
        this->state = WHEEL_BACKWARD_LEFT;
    this->moveAlongPoints_new( this->listOfPoints_tripod_backwardWheelLeft, this->midPointStep_tripod_backwardWheelLeft,4); 
}
void Kinematic::wheel_backward_right()
{
    if ( this->state != WHEEL_BACKWARD_RIGHT )
        this->state = WHEEL_BACKWARD_RIGHT;
    this->moveAlongPoints_new( this->listOfPoints_tripod_backwardWheelRight, this->midPointStep_tripod_backwardWheelRight,4); 
}
void Kinematic::auto_mode()
{
    if ( this->state != AUTO )
        this->state = AUTO;
    if ( uc->readOnce() > 30.0)
    {
        this->move_forward();
    }
    else
    {
        this->rotate_left();
        this->rotate_left();
        this->rotate_left();   
    }
}



void Kinematic :: stand()
{
    if (this->state == STAND )
    {
        
        return;
    }
    else if ( this->state == IDLE)
    {
        Point * leg0 = new Point (InitialPosXLeg0, InitialPosYLeg0,InitialPosZLeg0-*(this->posZ));
        Point * leg1 = new Point (InitialPosXLeg1, InitialPosYLeg1,InitialPosZLeg1-*(this->posZ));
        Point * leg2 = new Point (InitialPosXLeg2, InitialPosYLeg2,InitialPosZLeg2-*(this->posZ));
        Point * leg3 = new Point (InitialPosXLeg3, InitialPosYLeg3,InitialPosZLeg3-*(this->posZ));
        Point * leg4 = new Point (InitialPosXLeg4, InitialPosYLeg4,InitialPosZLeg4-*(this->posZ));
        Point * leg5 = new Point (InitialPosXLeg5, InitialPosYLeg5,InitialPosZLeg5-*(this->posZ));
    
        double degList[NUM_OF_SERVO];
        Point * pointList[NUM_OF_LEG] = { leg0, leg1, leg2, leg3, leg4, leg5 };
        legsIK(pointList,currentDegList);
        this->sc->runAllServo_new(currentDegList);
        
        usleep(250000);
        
        int originalInterval = *(this->interval);
        *(this->interval) = 150000;
        
        Point * calibrate_two[NUM_OF_LEG] = { leg0,leg1->femurRotate(-30),leg2,leg3,leg4,leg5};
        legsIK(calibrate_two,degList);
        this->moveToDegs(degList,*(this->interval));
        //usleep(50000);
        legsIK(pointList,degList);
        this->moveToDegs(degList,*(this->interval));
        
        
        Point * calibrate_six[NUM_OF_LEG] = { leg0,leg1,leg2,leg3,leg4,leg5->femurRotate(30)};
        legsIK(calibrate_six,degList);
        this->moveToDegs(degList,*(this->interval));
        //usleep(50000);
        legsIK(pointList,degList);
        this->moveToDegs(degList,*(this->interval));
        
        
        Point * calibrate_one[NUM_OF_LEG] = { leg0->femurRotate(-30),leg1,leg2,leg3,leg4,leg5};
        legsIK(calibrate_one,degList);
        this->moveToDegs(degList,*(this->interval));    
        //usleep(150000);
        legsIK(pointList,degList);
        this->moveToDegs(degList,*(this->interval));
      
        
        Point * calibrate_five[NUM_OF_LEG] = { leg0,leg1,leg2,leg3,leg4->femurRotate(30),leg5};
        legsIK(calibrate_five,degList);
        this->moveToDegs(degList,*(this->interval));
        //usleep(150000);
        legsIK(pointList,degList);
        this->moveToDegs(degList,*(this->interval));
  
   
        
        Point * calibrate_three[NUM_OF_LEG] = { leg0,leg1,leg2->femurRotate(-30),leg3,leg4,leg5};
        legsIK(calibrate_three,degList);
        this->moveToDegs(degList,*(this->interval));
        //usleep(150000);
        legsIK(pointList,degList);
        this->moveToDegs(degList,*(this->interval));
       
    
        Point * calibrate_four[NUM_OF_LEG] = { leg0,leg1,leg2,leg3->femurRotate(30),leg4,leg5};
        legsIK(calibrate_four,degList);
        this->moveToDegs(degList,*(this->interval));
        //usleep(150000);
        legsIK(pointList,degList);
        this->moveToDegs(degList,*(this->interval));
      

        
        *(this->interval) = originalInterval;
        
        
        this->state = STAND;
    
    }
    else if ( this->state == ROTATE_LEFT || this->state == GO_RIGHT)
    {
        Point * leg0 = new Point (InitialPosXLeg0, InitialPosYLeg0,InitialPosZLeg0-*(this->posZ));
        Point * leg1 = new Point (InitialPosXLeg1, InitialPosYLeg1,InitialPosZLeg1-*(this->posZ));
        Point * leg2 = new Point (InitialPosXLeg2, InitialPosYLeg2,InitialPosZLeg2-*(this->posZ));
        Point * leg3 = new Point (InitialPosXLeg3, InitialPosYLeg3,InitialPosZLeg3-*(this->posZ));
        Point * leg4 = new Point (InitialPosXLeg4, InitialPosYLeg4,InitialPosZLeg4-*(this->posZ));
        Point * leg5 = new Point (InitialPosXLeg5, InitialPosYLeg5,InitialPosZLeg5-*(this->posZ));
    
        double degList[NUM_OF_SERVO];
        Point * pointList[NUM_OF_LEG] = { leg0, leg1, leg2, leg3, leg4, leg5 };
       // legsIK(pointList,currentDegList);
       // this->sc->runAllServo_new(currentDegList);
        
        Point * calibrate_two[NUM_OF_LEG] = { leg0->femurRotate(-40),leg1,leg2->femurRotate(-40),leg3,leg4->femurRotate(40),leg5};
        legsIK(calibrate_two,degList);
        this->moveToDegs(degList,*(this->interval));
        //usleep(50000);
        legsIK(pointList,degList);
        this->moveToDegs(degList,*(this->interval));
        
        usleep(250000);
        
        Point * calibrate_one[NUM_OF_LEG] = { leg0,leg1->femurRotate(-40),leg2,leg3->femurRotate(40),leg4,leg5->femurRotate(40)};
        legsIK(calibrate_one,degList);
        this->moveToDegs(degList,*(this->interval));
        //usleep(50000);
        legsIK(pointList,degList);
        this->moveToDegs(degList,*(this->interval));
        
        
        
        
        
      

        
 
        
        
        this->state = STAND;
    }
    else
    {
        Point * leg0 = new Point (InitialPosXLeg0, InitialPosYLeg0,InitialPosZLeg0-*(this->posZ));
        Point * leg1 = new Point (InitialPosXLeg1, InitialPosYLeg1,InitialPosZLeg1-*(this->posZ));
        Point * leg2 = new Point (InitialPosXLeg2, InitialPosYLeg2,InitialPosZLeg2-*(this->posZ));
        Point * leg3 = new Point (InitialPosXLeg3, InitialPosYLeg3,InitialPosZLeg3-*(this->posZ));
        Point * leg4 = new Point (InitialPosXLeg4, InitialPosYLeg4,InitialPosZLeg4-*(this->posZ));
        Point * leg5 = new Point (InitialPosXLeg5, InitialPosYLeg5,InitialPosZLeg5-*(this->posZ));
    
        double degList[NUM_OF_SERVO];
        Point * pointList[NUM_OF_LEG] = { leg0, leg1, leg2, leg3, leg4, leg5 };
       // legsIK(pointList,currentDegList);
       // this->sc->runAllServo_new(currentDegList);
        
        //usleep(250000);
        
        Point * calibrate_one[NUM_OF_LEG] = { leg0,leg1->femurRotate(-40),leg2,leg3->femurRotate(40),leg4,leg5->femurRotate(40)};
        legsIK(calibrate_one,degList);
        this->moveToDegs(degList,*(this->interval));
        //usleep(50000);
        legsIK(pointList,degList);
        this->moveToDegs(degList,*(this->interval));
        
        usleep(250000);
        
        Point * calibrate_two[NUM_OF_LEG] = { leg0->femurRotate(-40),leg1,leg2->femurRotate(-40),leg3,leg4->femurRotate(40),leg5};
        legsIK(calibrate_two,degList);
        this->moveToDegs(degList,*(this->interval));
        //usleep(50000);
        legsIK(pointList,degList);
        this->moveToDegs(degList,*(this->interval));
        
      

        
 
        
        
        this->state = STAND;
    }
}


void Kinematic::bodyIK ( Point* pointList[] )
{
    for ( int i  =  0 ; i < NUM_OF_LEG ; i++ )
    {
        double totalX = this->init_leg_points[i]->x  + this->coxa_body_center_offsetX[i]  + *(this->posX);
        double totalY = this->init_leg_points[i]->y  + this->coxa_body_center_offsetY[i]  + *(this->posY);
        double totalZ = this->init_leg_points[i]->z *-1;
        double sinRotX = sin(*(this->rotX)*PI/180);
        double cosRotX = cos(*(this->rotX)*PI/180);
        double sinRotY = sin(*(this->rotY)*PI/180);
        double cosRotY = cos(*(this->rotY)*PI/180);
        double sinRotZ = sin(*(this->rotZ)*PI/180);
        double cosRotZ = cos(*(this->rotZ)*PI/180);
        double bodyIkX = totalX*cosRotZ*cosRotY - totalY*cosRotY*sinRotZ + totalZ*sinRotY - totalX;
        double bodyIkY1 = totalX*cosRotX*sinRotZ;
        double bodyIkY2 = totalX*cosRotZ*sinRotY*sinRotX;
        double bodyIkY3 = totalY*cosRotZ*cosRotX;
        double bodyIkY4 = totalY*sinRotZ*sinRotY*sinRotX;
        double bodyIkY5 = totalZ*cosRotY*sinRotX;
        double bodyIkY = bodyIkY1 + bodyIkY2 + bodyIkY3 - bodyIkY4 - bodyIkY5 - totalY;
        double bodyIkZ1 = totalX*sinRotZ*sinRotX;
        double bodyIkZ2 = totalX*cosRotZ*cosRotX*sinRotY ;
        double bodyIkZ3 =  totalY*cosRotZ*sinRotX ;
        double bodyIkZ4 = totalY*cosRotX*sinRotZ*sinRotY;
        double bodyIkZ5 = totalZ*cosRotY*cosRotX;
        double bodyIkZ = bodyIkZ1-bodyIkZ2+bodyIkZ3+bodyIkZ4+bodyIkZ5-totalZ;
        
        double newPosX = (this->init_leg_points[i]->x + *(this->posX) + bodyIkX);
        double newPosY = (this->init_leg_points[i]->y + *(this->posY) + bodyIkY);
        double newPosZ = (this->init_leg_points[i]->z*-1 + *(this->posZ) + bodyIkZ)*-1;
   
        
        pointList[i] = new Point (newPosX,newPosY,newPosZ) ;
        
        
    }
}

void Kinematic::legsIK( Point * pointList[] , double degreeList[] )
{
  
        //int tempDegList[NUM_OF_SERVO]; 
  
        for ( int j = 0 ; j < NUM_OF_LEG ; j++ )
        {
            Point * thePoint = pointList[j];
            calDeg_threeSer_new(thePoint->x,thePoint->y,thePoint->z,
                &degreeList[j*3],&degreeList[j*3+1],&degreeList[j*3+2]);
           
            
        }

        
    
}


void Kinematic::move_forward()
{
    if ( this->state != GO_FORWARD )
        this->state = GO_FORWARD;
    if ( *(this->kine_gait) == TRIPOD )
    {
       
        this->moveAlongPoints_new( this->listOfPoints_tripod_forward , this->midPointStep_tripod_forward,4);
    }
 
     
}
void Kinematic::move_backward()
{
    if ( this->state != GO_BACKWARD )
        this->state = GO_BACKWARD;
    if ( *(this->kine_gait) == TRIPOD )
    {
       
        this->moveAlongPoints_new( this->listOfPoints_tripod_backward, this->midPointStep_tripod_backward,4);
    }
}
void Kinematic::move_left()
{
    if ( this->state != GO_LEFT )
        this->state = GO_LEFT;
    this->moveAlongPoints_new( this->listOfPoints_crab_left, this->midPointStep_crab_left,4);
}
void Kinematic::move_right()
{
    if ( this->state != GO_RIGHT )
        this->state = GO_RIGHT;
    if ( *(this->kine_gait) == TRIPOD )
    {
       
        this->moveAlongPoints_new( this->listOfPoints_crab_right, this->midPointStep_crab_right,4);
    }
}


void Kinematic::calDeg_threeSer_new( double x , double y , double z , double *deg1, double *deg2, double *deg3)
{
     
   
   double degCoxa, degFemur, degTibia;
   double legLength;
   double HF;
   double A1,A2;
   double B1;
   
   z *= -1;
   
  
   degCoxa = atan2 (y,x) * 180 / PI; //*180/Pi for converting radian to degree
  
   //calibration for Gamma

   if ( degCoxa <= 0 )
       degCoxa += 180;
  // cout << "degCoxa: " << degCoxa << endl;
   
   
   legLength = sqrt( pow(x,2.0) + pow(y,2.0) );
  // cout << "legLength: " << legLength << endl;
   HF = sqrt( pow(legLength-Coxa_Length,2.0) + pow(z,2.0) );
  // cout << "HF: " << HF << endl;
   A1 = atan2( (legLength-Coxa_Length),z ) * 180/PI;
  // cout << "A1: " << A1 << endl;
   A2 = acos( ( pow(Tibia_Length,2.0) - pow(Femur_Length,2.0) - pow(HF,2.0) ) / (-2*Femur_Length*HF) ) * 180/PI;
  // cout << "A2: " << A2 << endl;
   degFemur = A1+A2;
   //cout << "degFemur: " << degFemur << endl;
   
   
   B1 = acos( ( pow(HF,2.0) - pow(Tibia_Length,2.0) - pow(Femur_Length,2.0) ) / (-2*Femur_Length*Tibia_Length) ) * 180/PI;
  // cout << "B1: " << B1 << endl;
   degTibia = B1;
   //cout << "degTibia: " << degTibia << endl;
   if ( isnan(degCoxa) || isnan(degFemur) || isnan(degTibia) || degCoxa > 180 || degFemur > 180 || degTibia > 180 )
   {
       cout << "Error_calDeg: cannot calculate the degree, return natural pos";
       double natural = 90.0;
       *deg1 = natural;
       *deg2 = natural;
       *deg3 = natural;
   }
   else
   {
        *deg1 = degCoxa;
        *deg2 = degFemur;
        *deg3 = degTibia;
   }
   
}





void Kinematic::moveAlongPoints_new ( Point * listOfPoints[][NUM_OF_LEG] , bool midPointStep[], int lengthOfList )
{
   // cout << "Enter moveAlongPoints_new, length of list:" <<  lengthOfList << endl;
    double listOfDegrees[lengthOfList][NUM_OF_SERVO];
    for ( int i = 0 ; i < lengthOfList; i++ )
    {
        //double degreeList[NUM_OF_SERVO];
        int pos = 0;
        for ( int j = 0 ; j < NUM_OF_SERVO ; j+=3 )
        {
            Point * thePoint = listOfPoints[i][pos];
            //cout << "the Point :" << thePoint->x << "," << thePoint->y << "," << thePoint->z << endl;
            calDeg_threeSer_new(thePoint->x,thePoint->y,thePoint->z,
                &listOfDegrees[i][j],&listOfDegrees[i][j+1],&listOfDegrees[i][j+2]);
            pos++;
        }
        
    }
   
    
    //cout << "going to move btw degree" << endl;
    
  
    for (  int i = 0 ; i < lengthOfList ; i++ )
    {
        //cout << "gait point step: " << i << endl;
        
        moveToDegs(listOfDegrees[i] , *(this->interval));
        if ( !midPointStep[i] )
        {
            usleep(*(this->gait_point_delay));
        }
        
    }
    
}

void Kinematic::moveToDegs( double toDegList[] , int time ) // time(in us) means how  long to finish the motion; 
{
    bool reachedNext[NUM_OF_SERVO] = { false };
   
  
    
    double partitionList[NUM_OF_SERVO];
    for ( int i = 0; i < NUM_OF_SERVO; i++ )
    {
        partitionList[i] = abs(toDegList[i]-this->currentDegList[i])* (*this->delay) *1.0/time;
        
    }
    int numOfLoop = time/(*this->delay);
   
    for ( int j = 0 ; j< numOfLoop ; j++ )
    {
        
      
      
        for ( int i = 0 ; i < NUM_OF_SERVO ; i++ )
        {
            
                if( this->currentDegList[i] < toDegList[i])
                {
       
                    if ( this->currentDegList[i]+ partitionList[i] > toDegList[i])
                    {
                        this->currentDegList[i] += (toDegList[i] - this->currentDegList[i] );
                    
                    }
                    else
                    {
                        this->currentDegList[i] += partitionList[i];
                    }
          
                }
                else if ( this->currentDegList[i] > toDegList[i])
                {
 
                    if ( this->currentDegList[i]-partitionList[i] < toDegList[i])
                    {
                        this->currentDegList[i] -= (this->currentDegList[i] - toDegList[i] );
                    
                    }
                    else
                    {
                        this->currentDegList[i] -= partitionList[i];
                    }
                }
                else if ( this->currentDegList[i] == toDegList[i] )
                {
        
                    if ( reachedNext[i] == false )
                    {
                  
                        reachedNext[i] = true;
                    }
                }
        }
        
                
            usleep(*(this->delay));
            this->sc->runAllServo_new(this->currentDegList);
        
           
    }
    
}







int Kinematic::getAction ()
{
    return *(this->action);
}