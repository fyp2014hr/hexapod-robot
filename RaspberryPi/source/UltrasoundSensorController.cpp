#include "/home/arack/git/hexapod-robot/RaspberryPi/header/GPIOClass.h"
#include "/home/arack/git/hexapod-robot/RaspberryPi/header/UltrasoundSensorController.h"
#include <sys/time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <iostream>

using namespace std;

UltrasoundSensorController::UltrasoundSensorController()
{
    
    this->trig_gpio = new GPIOClass("16", "RP");
    this->echo_gpio = new GPIOClass("12", "RP");
    this->delay = 500000;
}

UltrasoundSensorController::UltrasoundSensorController( string trigPin, string echoPin, string boardType )
{
    
    this->trig_gpio = new GPIOClass(trigPin, boardType);
    this->echo_gpio = new GPIOClass(echoPin, boardType);
    this->delay = 500000;
}

int UltrasoundSensorController::turnOn()
{
    
    this->trig_gpio->export_gpio();
    this->echo_gpio->export_gpio();
    
    this->trig_gpio->setDir("out");
    this->echo_gpio->setDir("in");
    
    this->trig_gpio->openValFile('w');
    this->echo_gpio->openValFile('r');
    
    this->trig_gpio->setVal("0");
    usleep(2000000);
    
    return 0;
}

double UltrasoundSensorController::readOnce()
{
  
    
    
    
    this->trig_gpio->setVal("1");
    usleep(10);
    this->trig_gpio->setVal("0");
    
    string echoVal;
    struct timeval tv_start;
    struct timeval tv_end;
    int start;
    int end;
    while ((echoVal=echo_gpio->getVal()) == "0")
    {
        gettimeofday (&tv_start, NULL);
        start = tv_start.tv_sec * 1000000 + tv_start.tv_usec;
    }
    while ((echoVal=echo_gpio->getVal()) == "1")
    {
        gettimeofday (&tv_end, NULL);
        end = tv_end.tv_sec * 1000000 + tv_end.tv_usec;
    }
    double distance = (end-start)/58.0;
    return distance;
    
}

int UltrasoundSensorController::turnOff()
{
    this->trig_gpio->closeValFile();
    this->echo_gpio->closeValFile();
    
    this->trig_gpio->unexport_gpio();
    this->echo_gpio->unexport_gpio();
    return 0;
}

int UltrasoundSensorController::startDetectObstacle()
{
    while ( true )
    {
        double distance = this->readOnce();
        cout << "The closest object is " << distance << " cm apart." << endl;
        usleep(this->delay);
    }
}

