/* 
 * File:   Point.h
 * Author: arack
 *
 * Created on January 19, 2015, 6:44 PM
 */

#ifndef POINT_H
#define	POINT_H

class Point
{
public:
    
    Point();
    Point(double x, double y, double z );
    double x;
    double y;
    double z;
    
    Point * translateX ( double extend );
    Point * translateY ( double extend );
    void translateZ ( double extend );
    Point * coxaRotate(double angle);
    Point * femurRotate(double angle);
    Point * tibiaRotate(double angle);
    //void rotate
    
};


#endif	/* POINT_H */

