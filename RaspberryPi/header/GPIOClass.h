/* 
 * File:   GPIOClass.h
 * Author: arack
 *
 * Created on September 20, 2014, 12:37 AM
 */

#ifndef GPIOCLASS_H
#define	GPIOCLASS_H

#include <string>
using namespace std;
/* GPIO Class
 * Purpose: Each object instantiated from this class will control a GPIO pin
 * The GPIO pin number must be passed to the overloaded class constructor
 */
class GPIOClass
{
public:
    GPIOClass();  // create a GPIO object that controls GPIO4 (default
    GPIOClass(string x,string boardType); // create a GPIO object that controls GPIOx, where x is passed to this constructor
    int export_gpio(); // exports GPIO
    int unexport_gpio(); // unexport GPIO
   
    int openValFile(char mode);
   
    int closeValFile();
    int setDir(string dir); // Set GPIO Direction
    int setVal(string val); // Set GPIO Value (putput pins)
    string getVal(); // Get GPIO Value (input/ output pins)
    string getNum(); // return the GPIO number associated with the instance of an object
private:
    string gpioNum; // GPIO number associated with the instance of an object
    string boardType; // the type of the board ( rasperri pi or wrtnode)
    int valFile;
};


#endif	/* GPIOCLASS_H */

