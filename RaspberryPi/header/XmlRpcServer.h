/* 
 * File:   XmlRpcServerClass.h
 * Author: arack
 *
 * Created on December 29, 2014
 */

#ifndef XMLRPCSERVER_H
#define	XMLRPCSERVER_H

#include <string>
#include "/home/arack/git/hexapod-robot/RaspberryPi/header/Kinematic.h"
#include "/home/arack/git/hexapod-robot/RaspberryPi/header/UltrasoundSensorController.h"
#include "/home/arack/git/hexapod-robot/RaspberryPi/header/Speak.h"
#include "/home/arack/git/hexapod-robot/RaspberryPi/header/PowerClipController.h"
#include <xmlrpc-c/base.h>
#include <xmlrpc-c/server.h>
#include <xmlrpc-c/server_abyss.h>
using namespace std;

extern int kine_action_shmId;
extern int kine_gait_shmId;
extern int kine_delay_shmId;
extern int kine_interval_shmId;
extern int kine_gait_point_delay_shmId;
extern double kine_posX_shmId;
extern double kine_posY_shmId;
extern double kine_posZ_shmId;
extern double kine_rotX_shmId;
extern double kine_rotY_shmId;
extern double kine_rotZ_shmId;
extern int kine_flag_motionDirty_shmId;

extern ServoController * sc;
extern Kinematic * kine;
extern UltrasoundSensorController * uc;
extern Speak * sk;
extern PowerClipController * pcc;

extern xmlrpc_env xml_client_env;
extern xmlrpc_client * xml_clientP;




class XmlRpcServer
{
public:
    XmlRpcServer();  
    XmlRpcServer(int port );
    int start();
    int stop();
    bool isRunning;
    
    
    
private:
    int port;
    
    
    
    
    
};


#endif	
