/* 
 * File:   Kinematic.h
 * Author: arack
 *
 * Created on January 9, 2015, 9:37 PM
 */

#ifndef KINEMATIC_H
#define	KINEMATIC_H
#include <string>
#include "/home/arack/git/hexapod-robot/RaspberryPi/header/ServoController.h"
#include "/home/arack/git/hexapod-robot/RaspberryPi/header/Point.h"
#include "/home/arack/git/hexapod-robot/RaspberryPi/header/CommonResource.h"
#include "/home/arack/git/hexapod-robot/RaspberryPi/header/UltrasoundSensorController.h"
#include <vector>

#define PI 3.14159265

// ALL length unit is in mm
#define Coxa_Length 32
#define Femur_Length 55
#define Tibia_Length 125

#define IDLE 0
#define STAND 1
#define GO_FORWARD 2
#define GO_BACKWARD 3
#define GO_LEFT 4
#define GO_RIGHT 5
#define DANCE 6
#define ROTATE_LEFT 7
#define ROTATE_RIGHT 8
#define WHEEL_FORWARD_LEFT 9
#define WHEEL_FORWARD_RIGHT 10
#define WHEEL_BACKWARD_LEFT 11
#define WHEEL_BACKWARD_RIGHT 12
#define AUTO 13
#define MUSIC_DANCE 14
#define DEMO 15


//gait
#define TRIPOD 7

//Body IK
#define BodyCoxaOffsetX_leg0 82.0
#define BodyCoxaOffsetX_leg1 0.0
#define BodyCoxaOffsetX_leg2 -81.0
#define BodyCoxaOffsetX_leg3 82.0
#define BodyCoxaOffsetX_leg4 0.0 
#define BodyCoxaOffsetX_leg5 -82.0

#define BodyCoxaOffsetY_leg0 -56.0
#define BodyCoxaOffsetY_leg1 -60.0
#define BodyCoxaOffsetY_leg2 -59.0
#define BodyCoxaOffsetY_leg3 59.0
#define BodyCoxaOffsetY_leg4 62.0
#define BodyCoxaOffsetY_leg5 61.5

#define InitialPosXLeg0 61.52
#define InitialPosYLeg0 -61.52
#define InitialPosZLeg0 -125.0
#define InitialPosXLeg1 0.0
#define InitialPosYLeg1 -87.0
#define InitialPosZLeg1 -125.0
#define InitialPosXLeg2 -61.52
#define InitialPosYLeg2 -61.52
#define InitialPosZLeg2 -125.0
#define InitialPosXLeg3 61.52
#define InitialPosYLeg3 61.52
#define InitialPosZLeg3 -125.0
#define InitialPosXLeg4 0.0
#define InitialPosYLeg4 87.0
#define InitialPosZLeg4 -125.0
#define InitialPosXLeg5 -61.52
#define InitialPosYLeg5 61.52
#define InitialPosZLeg5 -125.0




extern int kine_action_shmId;
extern int kine_delay_shmId;
extern int kine_interval_shmId;
extern int kine_gait_point_delay_shmId;
extern int kine_gait_shmId;
extern double kine_posX_shmId;
extern double kine_posY_shmId;
extern double kine_posZ_shmId;
extern double kine_rotX_shmId;
extern double kine_rotY_shmId;
extern double kine_rotZ_shmId;
extern int kine_flag_motionDirty_shmId;


extern UltrasoundSensorController * uc;






using namespace std;

class Kinematic
{
    public:
       
       
        

        Kinematic( int delay, int partition, ServoController * sc );
        
        void calDeg_threeSer_new( double x , double y , double z , double *deg1, double *deg2, double *deg3);
        
   
        void stand();
        void move_forward( );
        void move_backward();
        void move_left();
        void move_right();
        void move_forward_tripod();
        void music_dance();
        void demo();
        void dance();
        void wheel_forward_left();
        void wheel_forward_right();
        void wheel_backward_left();
        void wheel_backward_right();
        void rotate_left();
        void rotate_right();
        void auto_mode();

        void legsIK( Point * pointList[] , double degreeList[] );
  
        void moveAlongPoints_new ( Point * listOfPoints[][NUM_OF_LEG] ,  bool midPointStep[], int lengthOfList );
     
        
        void moveToDegs ( double toDegList[] , int time );
  
        
   
        
        void init_bodyIK_data();
       
        int getAction();
        void cal_gait_coordinate();
        
        //body ik
        void bodyIK(Point * pointList[]);
        
        //body Ik
        int * motionDirty;
        
        double * posX;
        double * posY;
        double * posZ;
        double * rotX;
        double * rotY;
        double * rotZ;
        int * action;
        
        Point * natural_gait_left;
        Point * natural_gait_right;
      
        double currentDegList[NUM_OF_SERVO];
        int state;
    private:
        
        
        
        int * delay;
        int * interval;
        int * gait_point_delay;
        int * kine_gait;
        vector<ServoController *> sccs;
        ServoController * sc;
        
        //gait
        Point * listOfPoints_tripod_forward [4][NUM_OF_LEG];
        bool midPointStep_tripod_forward[4];
        Point * listOfPoints_tripod_backward [4][NUM_OF_LEG];
        bool midPointStep_tripod_backward[4];
        Point * listOfPoints_crab_right [4][NUM_OF_LEG];
        bool midPointStep_crab_right[4];
        Point * listOfPoints_crab_left [4][NUM_OF_LEG];
        bool midPointStep_crab_left[4];
        Point * listOfPoints_rotate_left [4][NUM_OF_LEG];
        bool midPointStep_rotate_left[4];
        Point * listOfPoints_rotate_right [4][NUM_OF_LEG];
        bool midPointStep_rotate_right[4];
        
        Point * listOfPoints_tripod_forwardWheelRight[4][NUM_OF_LEG];
        bool midPointStep_tripod_forwardWheelRight[4];
        Point * listOfPoints_tripod_backwardWheelRight[4][NUM_OF_LEG];
        bool midPointStep_tripod_backwardWheelRight[4];
        Point * listOfPoints_tripod_forwardWheelLeft[4][NUM_OF_LEG];
        bool midPointStep_tripod_forwardWheelLeft[4];
        Point * listOfPoints_tripod_backwardWheelLeft[4][NUM_OF_LEG];
        bool midPointStep_tripod_backwardWheelLeft[4];
        
        
        
        
        
        // for bodyIK
        Point * init_leg_points[NUM_OF_LEG];
        double coxa_body_center_offsetX[NUM_OF_LEG];
        double coxa_body_center_offsetY[NUM_OF_LEG];
        
       
        
    
};



#endif	/* KINEMATIC_H */