/* 
 * File:   CommonResource.h
 * Author: arack
 *
 * Created on February 5, 2015, 10:46 PM
 */

#ifndef COMMONRESOURCE_H
#define	COMMONRESOURCE_H

#define NUM_OF_SERVO 18
#define NUM_OF_LEG 6

#define XMLRPC_SERVER1 "http://localhost:9191/RPC2"
#define XMLRPC_SERVER2 "http://192.168.1.2:8080/RPC2"

#define SERVO_RUN_ALL_METHOD "servo_run_all"
#define SERVO_RUN_METHOD "servo_run"
#define SERVO_POWER_OFF_METHOD "servo_power_off"


#define PI 3.14159265

#define NAME "Xmlrpc-c Test Client"
#define VERSION "1.0" 

#define MEGEXTRA 1000000

//--espeak---
#define SPEAK_LAUNCHUP "The Most Intelligent Hexapod on duty! Please wait for setting up."



#endif	/* COMMONRESOURCE_H */

