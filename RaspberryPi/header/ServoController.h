/* 
 * File:   ServoController.h
 * Author: arack
 *
 * Created on November 25, 2014, 12:16 AM
 */

#ifndef SERVOCONTROLLER_H
#define	SERVOCONTROLLER_H

#include <string>
#include <xmlrpc-c/base.h>
#include <xmlrpc-c/client.h> 
#include <map>
#include "/home/arack/git/hexapod-robot/RaspberryPi/header/GPIOClass.h"
#include "/home/arack/git/hexapod-robot/RaspberryPi/header/CommonResource.h"

/*
#define SER1 0
#define SER2 1
#define SER3 2
#define SER4 3
#define SER5 4
#define SER6 5
#define SER7 6
#define SER8 7
#define SER9 8
#define SER10 0
#define SER11 1
#define SER12 2
#define SER13 3
#define SER14 4
#define SER15 5
#define SER16 6
#define SER17 7
#define SER18 8

*/
#define INDEX_MAP

/* servo degree offet */
#define OFFSET0 -5
#define OFFSET1 -2
#define OFFSET2 -2
#define OFFSET3 4
#define OFFSET4 4
#define OFFSET5 -5
#define OFFSET6 -7
#define OFFSET7 -5
#define OFFSET8 -1
#define OFFSET9 0
#define OFFSET10 2
#define OFFSET11 3
#define OFFSET12 0 
#define OFFSET13 -2
#define OFFSET14 -2
#define OFFSET15 -2
#define OFFSET16 -2
#define OFFSET17 0

/* if servo is reversed*/
#define ISREVERSED0 true
#define ISREVERSED1 true
#define ISREVERSED2 false
#define ISREVERSED3 true
#define ISREVERSED4 true
#define ISREVERSED5 false
#define ISREVERSED6 true
#define ISREVERSED7 true
#define ISREVERSED8 false
#define ISREVERSED9 true
#define ISREVERSED10 false
#define ISREVERSED11 true
#define ISREVERSED12 true
#define ISREVERSED13 false
#define ISREVERSED14 true
#define ISREVERSED15 true
#define ISREVERSED16 false
#define ISREVERSED17 true


#define SERVO_POWER_SWITCH_PIN "15"
#define SERVO_POWER_SWITCH_PIN2 "16"



extern xmlrpc_env xml_client_env;
extern xmlrpc_client * xml_clientP;


using namespace std;

class ServoController
{
public:
    ServoController();  
    ServoController(string boardType);
 
    int setPulse(int index , int pulse);
   
    int runOneServo_new(int index, double degree );

    int runAllServo_new(double degree[]);
    void calibrateDegree ( int index , double * degree );
    

    int powerOff();
    int powerOn();
    
    
    
    
    
private:
    string dir;
    string boardType;
    string pwmNumber;

   
    
    map<int , string >pathMap;
    map<int , FILE *>fileMap;
    
    GPIOClass* powerSwitch;
    GPIOClass* powerSwitch2;

    
};


#endif	/* SERVOCONTROLLERCLASS_H */

