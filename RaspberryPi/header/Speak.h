
/*
 * File:   Speak.h
 * Author: arack
 *
 * Created on January 9, 2015, 11:00 PM
 */

#ifndef SPEAK_H
#define	SPEAK_H

#include <string>
using namespace std;


class Speak
{
    public:
        Speak();
        void speakOnce( string s);
};




#endif	/* SPEAK_H */

