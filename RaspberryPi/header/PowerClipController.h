/* 
 * File:   PowerClipController.h
 * Author: arack
 *
 * Created on March 24, 2015, 6:06 PM
 */

#ifndef POWERCLIPCONTROLLER_H
#define	POWERCLIPCONTROLLER_H
#include <string>


#define PCC_PWM_LEFT "2"
#define PCC_PWM_RIGHT "3"

using namespace std;


class PowerClipController
{
    public:
        PowerClipController(string pwmLeft, string pwmRight);  
        void tight();
        void release();
    private:
        string pwmLeft;
        string pwmRight;
};

#endif	/* POWERCLIPCONTROLLER_H */

