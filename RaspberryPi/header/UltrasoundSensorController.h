/* 
 * File:   UltrasoundSensorController.h
 * Author: arack
 *
 * Created on October 17, 2014, 12:50 AM
 */

#ifndef ULTRASOUNDSENSORCONTROLLER_H
#define	ULTRASOUNDSENSORCONTROLLER_H

#define TRIG_PIN "26"
#define ECHO_PIN "20"

#include <string>
#include "/home/arack/git/hexapod-robot/RaspberryPi/header/GPIOClass.h"
using namespace std;

class UltrasoundSensorController
{
public:
    UltrasoundSensorController();  
    UltrasoundSensorController(string trigPin, string echoPin, string boardType);
    int turnOn();
    int turnOff();
    int setDelay(int us);
    double readOnce();
    int startDetectObstacle();
private:
    //string trigPin; 
    //string echoPin; 
    int delay;
    
    GPIOClass* trig_gpio;
    GPIOClass* echo_gpio;
};


#endif	/* ULTRASOUNDSENSORCONTROLLERCLASS_H */

