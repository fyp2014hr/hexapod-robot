package bryce.fyp2015.hrcontroller;

import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.rey.material.widget.Button;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;

import bryce.fyp2015.hrcontroller.Joystick.DualJoystickView;
import bryce.fyp2015.hrcontroller.Joystick.JoystickMovedListener;
import bryce.fyp2015.hrcontroller.MJPEGStream.MjpegInputStream;
import bryce.fyp2015.hrcontroller.MJPEGStream.MjpegView;
import de.timroes.axmlrpc.XMLRPCClient;

/**
 * A simple {@link Fragment} subclass.
 */
public class BasicMode extends Fragment {

    SharedPreferences prefs;
    String robotAddress, webCamAddress, speakerAddress, speakerText;

    private static final String TAG = "BasicMode";
    private MjpegView mv;
    private View view;
    private TextView connectStatusText, ultraSoundReading, heightReading;
    private EditText speakerEditText;
    private Button speakerSend;
    private SeekBar heightSeek;

    private int currentAction = 1;
    private boolean suspending = false, debug, activityStop = false;

    public BasicMode() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_basic_mode, container, false);
        Log.e(TAG, "OnCreateView");
        // Inflate the layout for this fragment
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Log.e(TAG,"onActivityCreated()");
        setupSharedPreference();
        checkConnection();
        setupCamera();
        loadView();
        loadJoyStick();
        Log.e(TAG,"onActivity() to create ultraReading");
        ultraReading();
    }

    public void onPause() {
        Log.e(TAG,"onPause()");
        super.onPause();
        if(mv!=null){
            if(mv.isStreaming()){
                mv.stopPlayback();
                suspending = true;
            }
        }
        activityStop = true;
        ultraStop();
    }

    public void onResume() {
        Log.e(TAG,"onResume()" + suspending);
        super.onResume();
        if(mv!=null){
            if(suspending){
                Log.e(TAG,"onResume()" + suspending + "2");
                new DoRead().execute(webCamAddress);
                suspending = false;
            }
        }
        activityStop = false;
    }

    public void onStop() {
        activityStop = true;
        ultraStop();
        Log.e(TAG,"onStop()");
        super.onStop();
    }

    public void onDestroy() {
        Log.e(TAG,"onDestroy()" + (mv == null));
        activityStop = true;
        ultraStop();
        super.onDestroy();

    }


    public void loadView(){
        connectStatusText = (TextView) view.findViewById(R.id.ConnectionStatus);
        speakerEditText = (EditText) view.findViewById(R.id.speakerString);
        speakerSend = (Button) view.findViewById(R.id.speakerSend);
        speakerSend.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View view, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    //speakerSend.setBackgroundColor(getResources().getColor(R.color.primary));
                } else if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    //speakerSend.setBackgroundColor(getResources().getColor(R.color.primary_light));
                    speakerString();
                }
                return false;
            }

        });

        // SetOnTouchListener for the buttons on Basic Mode
        setTouchListener(R.id.rotateClockwise, 8);
        setTouchListener(R.id.rotateAnticlockwise, 7);

        setScreenCapture();

        heightSeek = (SeekBar) view.findViewById(R.id.heightSeek);
        heightSeek.setOnSeekBarChangeListener(_listenerH);
        heightReading = (TextView) view.findViewById(R.id.heightText);
        view.findViewById(R.id.resetHeight).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                heightSeek.setProgress(10);
                heightReading.setHint("-30");
                setMovement(6);
                setDoubleMovement("kine_set_posZ", -30);
            }
        });
    }

    public void setupSharedPreference(){
        prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());
        debug = prefs.getBoolean("DebugMode", false);
        robotAddress = prefs.getString("RobotAddress", " ");
        webCamAddress = prefs.getString("webCamAddress", "http://192.168.3.1:9002/") + "?action=stream";
        speakerAddress = prefs.getString("speakerAddress", " ");
    }


    public void setTouchListener (final int viewID, final int movement) {
        view.findViewById(viewID).setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch(event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        setMovement(movement);
//                        view.findViewById(viewID).setBackgroundColor(getResources().getColor(R.color.primary_light));
//                        return false; // if you want to handle the touch event
                    case MotionEvent.ACTION_UP:
                        setMovement(1);
//                        view.findViewById(viewID).setBackgroundColor(getResources().getColor(R.color.primary));
//                        return false; // if you want to handle the touch event
                }
                return true;
            }
        });
    }

    public void setMovement (final int direction) {

        Thread thread=new Thread(new Runnable()
        {
            @Override
            public void run()
            {
                try {
                    XMLRPCClient client = new XMLRPCClient(new URL(robotAddress));
                    client.call("kine_set_action" , direction);
                    Log.i("Set Position ", direction +"");
                } catch(Exception ex) {
                    Log.e("[Failed] Set Position ", direction +"");
                    Log.e("[Failed] Set Position ", ex.toString());
                }
            }

        });
        thread.start();
    }

    /*
    The following code should be refactor because they appear in every activity, fragment where
    connection to robot and webCam is required.
    */


    public void checkConnection(){
        Thread thread=new Thread(new Runnable()
        {
            @Override
            public void run()
            {
                try {
                    XMLRPCClient client = new XMLRPCClient(new URL(robotAddress));

//                    setMovement(6);
                    resetPosition();
                    setMovement(1);
                    client.call("servo_power_on");
//                    // Set the robot to stand
//
//                    TextToVoice("Robot is Ready");

                    Log.i("XML-RPC: ", "Connection to " + robotAddress + " is successful.");
                    getActivity().runOnUiThread(new Runnable() {
                        public void run()
                        {
                            //Toast.makeText(getActivity(), "Connection Successful.", Toast.LENGTH_LONG).show();
                            connectStatusText.setText("Connection Successful.");
                            connectStatusText.setTextColor(getResources().getColor(R.color.success_connect));
                        }
                    });
                } catch(Exception ex) {
                    Log.e("XML-RPC Address: ", robotAddress);
                    Log.e("XML-RPC Fail: ", ex.toString());
                }
            }

        });
        thread.start();
    }

    public void resetPosition() {
        // Reset the initial position of the robot
        setDoubleMovement("kine_set_posX", 0);
        setDoubleMovement("kine_set_posY", 0);
        setDoubleMovement("kine_set_posZ", -30);

        setDoubleMovement("kine_set_rotX", 0);
        setDoubleMovement("kine_set_rotY", 0);
        setDoubleMovement("kine_set_rotZ", 0);
    }

    // Following functions are speaker-related

    public void speakerString() {

        speakerText = speakerEditText.getText().toString();
        TextToVoice(speakerText);

    }

    public void TextToVoice(final String speakerText) {

        prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());
        speakerAddress = prefs.getString("speakerAddress", " ");

        Thread thread=new Thread(new Runnable()
        {
            @Override
            public void run()
            {
                try {
                    XMLRPCClient client = new XMLRPCClient(new URL(speakerAddress));
                    client.call("speak", speakerText + "");

                    Log.i("XML-RPC: ", "Send Speaker Text successful - " + speakerText);

                } catch(Exception ex) {
                    Log.e("XML-RPC: ",  ex.toString());
                    Log.e("XML-RPC: ", "Send Speaker Text failed - " + speakerText);
                    getActivity().runOnUiThread(new Runnable() {
                        public void run()
                        {
                            Toast.makeText(getActivity(), "String Sent Failed.", Toast.LENGTH_SHORT).show();
//                            speakerEditText.setHint("Sent Failed.");
                        }
                    });
                }
            }

        });
        thread.start();
    }

    // Following functions are camera-related.

    public void setupCamera() {
        // Set up the MJPEGView

        Log.e(TAG, webCamAddress);
        mv = (MjpegView) view.findViewById(R.id.mv);

            if(!suspending){
                new DoRead().execute(webCamAddress);
                suspending = false;
            }

    }

    public class DoRead extends AsyncTask<String, Void, MjpegInputStream> {
        protected MjpegInputStream doInBackground(String... url) {
            HttpResponse res;
            DefaultHttpClient httpclient = new DefaultHttpClient();
            Log.d(TAG, "1. Sending http request");
            try {
                res = httpclient.execute(new HttpGet(URI.create(url[0])));
                Log.d(TAG, "2. Request finished, status = " + res.getStatusLine().getStatusCode());
                if(res.getStatusLine().getStatusCode()==401){
                    //You must turn off camera User Access Control before this will work
                    return null;
                }
                return new MjpegInputStream(res.getEntity().getContent());
            } catch (ClientProtocolException e) {
                e.printStackTrace();
                Log.d(TAG, "Request failed-ClientProtocolException", e);
                //Error connecting to camera
            } catch (IOException e) {
                e.printStackTrace();
                Log.d(TAG, "Request failed-IOException", e);

                //Error connecting to camera
            }

            return null;
        }

        protected void onPostExecute(MjpegInputStream result) {
            mv.setSource(result);
            if(result!=null) {
                result.setSkip(1);
            }
            mv.setDisplayMode(MjpegView.SIZE_BEST_FIT);
            mv.showFps(true);

        }
    }

    // SeekBar implementation

    private SeekBar.OnSeekBarChangeListener _listenerH = new SeekBar.OnSeekBarChangeListener() {

        int progressChanged = 0;

        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser){
            progressChanged = progress;

        }

        public void onStartTrackingTouch(SeekBar seekBar) {
            // TODO Auto-generated method stub
        }

        public void onStopTrackingTouch(SeekBar seekBar) {
            //Toast.makeText(getActivity(),"seek bar progress:"+ (progressChanged - 60),
                    //Toast.LENGTH_SHORT).show();

            heightReading.setHint("" + (progressChanged - 40));
            setMovement(6);
            setDoubleMovement("kine_set_posZ", (progressChanged - 40));
        }
    };

    //JoyStick Implementation

    DualJoystickView sJoyStick;
    private int radiusL, angleL;
    private double radiusR, angleR;
    final static double positionMax = 30.0;

    public void loadJoyStick(){

        sJoyStick = (DualJoystickView)getActivity().findViewById(R.id.joystickTranslate);
        sJoyStick.setOnJostickMovedListener(_listenerL, _listenerR);
    }

    Activity mainActivity;

    private JoystickMovedListener _listenerL = new JoystickMovedListener() {

        public void OnMoved(int pan, int tilt) {

            mainActivity = getActivity();

            if(mainActivity instanceof MainActivity) {
                ((MainActivity) mainActivity).changeAutoTitle();
            }

            radiusL = (int) Math.sqrt((pan*pan) + (tilt*tilt));
            angleL = (int) ((Math.atan2(-pan, -tilt)) * 180 / Math.PI);
            currentAction =1;

            if (radiusL > 5) {
                if (angleL > -22.5 && angleL <= 22.5) {
                    setMovement(2);
                    Log.i("Joystick", "Forward");
                } else if (angleL > 22.5 && angleL <= 67.5) {
                    setMovement(9);
                    Log.i("Joystick", "Forward Left");
                } else if (angleL > 67.5 && angleL <= 112.5) {
                    setMovement(4);
                    Log.i("Joystick", "Left");
                } else if (angleL > 112.5 && angleL <= 157.5) {
                    setMovement(11);
                    Log.i("Joystick", "Backward Left");
                } else if (angleL > 157.5 || angleL < -157.5) {
                    setMovement(3);
                    Log.i("Joystick", "Backward");
                } else if (angleL < -22.5 && angleL >= -67.5) {
                    setMovement(10);
                    Log.i("Joystick", "Forward Right");
                } else if (angleL < -67.5 && angleL >= -112.5) {
                    setMovement(5);
                    Log.i("Joystick", "Right");
                } else if (angleL < -112.5 && angleL >= -157.5) {
                    setMovement(12);
                    Log.i("Joystick", "Backward Right");
                }
            }

        }

        public void OnReleased() {
            if (debug) Log.d(TAG, "[JoyStickL] On Release");
        }

        public void OnReturnedToCenter() {
            setMovement(1);
            if (debug) Log.d(TAG, "[JoyStickL] Return to Central: 1st Try");
            try{
                Thread.sleep(300);
            } catch (Exception ex) {
                Log.w("Sleep", ex.toString());
            }
            setMovement(1);
            if (debug) Log.d(TAG, "[JoyStickL] Return to Central: 2nd Try");
        }
    };

    private JoystickMovedListener _listenerR = new JoystickMovedListener() {

        public void OnMoved(int pan, int tilt) {
            radiusR = Math.sqrt((pan*pan) + (tilt*tilt));
            angleR = Math.atan2(-pan, -tilt);
            if (radiusR > 5) {
                if ( currentAction != 6)
                    setMovement(6);
                positionMovement(Math.min(radiusR, 10), angleR * 180 / Math.PI + 90);
                currentAction = 6;

            }
        }

        public void OnReleased() {
            if (debug) Log.d(TAG, "[JoyStickR] On Release");
            positionMovement(0, 0);
        }

        public void OnReturnedToCenter() {
            positionMovement(0, 0);
            if (debug) Log.d(TAG, "[JoyStickR] Return to Central: 1st Try");
            try{
                Thread.sleep(300);
            } catch (Exception ex) {
                Log.w(TAG, "[Sleep] " + ex.toString());
            }
            positionMovement(0, 0);
            if (debug) Log.d(TAG, "[JoyStickR] Return to Central: 2nd Try");
        }
    };


    //
    // This is ultraThread
    //

    Thread ultraThread;

    public void ultraReading() {
        ultraSoundReading = (TextView) view.findViewById(R.id.ultrasoundReading);
        ultraThread = new Thread(new Runnable()
        {
            @Override
            public void run()
            {
                while (!activityStop) {
                    //if (debug) {
                        java.util.Date now = new java.util.Date();
                        Log.d(TAG, "[UltraReading Start] " + new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(now));
                    //}
                    try {
                        XMLRPCClient client = new XMLRPCClient(new URL(robotAddress));

                        final double readings = (double) client.call("ultrasound_sensor_read");
                        if (readings > 0) {
                            getActivity().runOnUiThread(new Runnable() {
                                public void run() {
                                    ultraSoundReading.setText("Distance: " + String.format("%.0f", readings));
                                    //connectStatusText.setTextColor(getResources().getColor(R.color.success_connect));
                                }
                            });
                        }

                    } catch (Exception ex) {
                        Log.w(TAG, "[UltraReading] " + ex.toString());
                    }

                    try {
                        Thread.sleep(1000);
                        //if (debug) {
                            java.util.Date now2 = new java.util.Date();
                            Log.d(TAG, "[UltraReading End] " + new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(now2));
                        //}
                    } catch (Exception ex) {
                        Log.w(TAG, "[UltraReading] " + ex.toString());
                    }
                }
            }

        });
        ultraThread.start();
        Log.e(TAG,"ultraReading start");
    }

    public void ultraStop() {
        Log.d(TAG, "[ultraStop] Start!");
        if(ultraThread!=null){
//            boolean retry = true;
//            while(retry) {
//                try {
//                    ultraThread.join();
//                    retry = false;
//                } catch (InterruptedException e) {
//                    Log.e(TAG, "[stopPlayback] Error in Thread joining");
//                    Log.e(TAG, e.toString());
//                }
//            }
            ultraThread = null;
        }
        Log.d(TAG, "[ultraStop] Completed!");

    }

    public void positionMovement(double radius, double angle){
        Log.w("PositionMove", "Radius: "+ radius + "; Angle: "+angle);
        double x = radius * Math.cos(Math.toRadians(angle));
        double y = radius * Math.sin(Math.toRadians(angle));

        setDoubleMovement("kine_set_posY", x / 8 * positionMax);
        setDoubleMovement("kine_set_posX", (-1) * y / 8 * positionMax);
    }

    public void setDoubleMovement (final String methodCall, final double direction) {

        Thread thread=new Thread(new Runnable()
        {
            @Override
            public void run()
           {
                try {
                    XMLRPCClient client = new XMLRPCClient(new URL(robotAddress));

                    client.call(methodCall , direction);

                    Log.i("set Movement ", methodCall +"");
                    Log.i("Set Position ", direction +"");
                } catch(Exception ex) {
                    //TODO: to change for d to w after debug
                    Log.i("[Failed] Set Position ", direction +"");
                    Log.i("[Failed] Set Position ", ex.toString());
                }
            }

        });
        thread.start();

    }

    public void setScreenCapture(){
        view.findViewById(R.id.pink_icon).setOnClickListener(screenCapture);
    }

    View.OnClickListener screenCapture = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            Thread thread=new Thread(new Runnable()
            {
                @Override
                public void run()
                {
                    //Toast.makeText(getActivity(), "Clicked pink Floating Action Button", Toast.LENGTH_SHORT).show();
                    try {
                        saveImageToExternalStorage(getBitmapFromURL(prefs.getString("webCamAddress", "http://192.168.3.1:9002/") + "?action=snapshot"));
                    } catch (Exception ex) {
                        Log.e("Screen Capture", "failed");
                        Log.e("Screen Capture", "" + ex);
                    }
                }

            });
            thread.start();
        }
    };

    public static Bitmap getBitmapFromURL(String src) {
        try {
            URL url = new URL(src);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap myBitmap = BitmapFactory.decodeStream(input);
            return myBitmap;
        } catch (IOException e) {
            e.printStackTrace();
            Log.e("Screen Capture", e.toString());
            return null;
        }
    }

    public boolean saveImageToExternalStorage(Bitmap image) throws IOException{
        final String APP_PATH_SD_CARD = "/HexaPodControl/screenshot";
        final String fullPath = Environment.getExternalStorageDirectory().getAbsolutePath() + APP_PATH_SD_CARD;

        final SimpleDateFormat sdFormat = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");
        final Date current = new Date();
        //System.out.println(sdFormat.format(current));

        Log.e("saveToExternalStorage()", "1");
        try {
            File dir = new File(fullPath);
            Log.e("saveToExternalStorage()", "2");
            if (!dir.exists()) {
                dir.mkdirs();
                Log.e("saveToExternalStorage()", "2.5");
            }

            OutputStream fOut = null;
            File file = new File(fullPath, "snapshot"+sdFormat.format(current)+".png");
            file.createNewFile();
            fOut = new FileOutputStream(file);
            Log.e("saveToExternalStorage()", "3");

            // 100 means no compression, the lower you go, the stronger the compression
            image.compress(Bitmap.CompressFormat.PNG, 100, fOut);
            fOut.flush();
            fOut.close();
            Log.e("saveToExternalStorage()", "4");

            MediaStore.Images.Media.insertImage(getActivity().getContentResolver(), file.getAbsolutePath(), file.getName(), file.getName());

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                Intent mediaScanIntent = new Intent(
                        Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
                Uri contentUri = Uri.fromFile(file); //out is your output file
                mediaScanIntent.setData(contentUri);
                getActivity().sendBroadcast(mediaScanIntent);
            } else {
                getActivity().sendBroadcast(new Intent(
                        Intent.ACTION_MEDIA_MOUNTED,
                        Uri.parse("file://" + Environment.getExternalStorageDirectory())));
            }
            Log.e("saveToExternalStorage()", "Done!");
            getActivity().runOnUiThread(new Runnable() {
                public void run() {
                    Toast.makeText(getActivity(), "Screen Captured as " + fullPath + "snapshot" + sdFormat.format(current) + ".png",
                            Toast.LENGTH_SHORT).show();
                }
            });

            return true;

        } catch (Exception e) {
            Log.e("saveToExternalStorage()", e.getMessage());
            return false;
        }
    }

}
