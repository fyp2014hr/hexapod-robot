package bryce.fyp2015.hrcontroller;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;


public class Landing extends Activity {

    SharedPreferences prefs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_landing);
        setButtonListener();
        loadRobotAddress();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_landing, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void sendAddress(View view) {
        Intent intent = new Intent(this, MainActivity.class);
        EditText editText = (EditText) findViewById(R.id.edit_address);
        String robotAddress = editText.getText().toString();

        // Save the address to DefaultSharedPreferences
        SharedPreferences.Editor prefEditor = prefs.edit();
        prefEditor.putString("RobotAddress", robotAddress);
        prefEditor.apply();

        startActivity(intent);
    }

    public void loadRobotAddress(){
        EditText editAddress = (EditText) findViewById(R.id.edit_address);
        prefs = PreferenceManager.getDefaultSharedPreferences(this);
        editAddress.setText(prefs.getString("RobotAddress", ""));
    }

    public void setButtonListener(){
        final Button button = (Button) findViewById(R.id.send_address);
        button.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View view, MotionEvent event) {
                if(event.getAction() == MotionEvent.ACTION_UP) {
                    button.setBackgroundColor(getResources().getColor(R.color.accent));
                } else if(event.getAction() == MotionEvent.ACTION_DOWN) {
                    button.setBackgroundColor(getResources().getColor(R.color.accent_pressed));
                }
                return false;
            }

        });
    }
}
