package bryce.fyp2015.hrcontroller;


import android.app.Fragment;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.preference.EditTextPreference;
import android.preference.ListPreference;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.Toast;

import java.net.URL;

import de.timroes.axmlrpc.XMLRPCClient;


/**
 * A simple {@link Fragment} subclass.
 */
public class Preference extends PreferenceFragment implements android.preference.Preference.OnPreferenceChangeListener {

    SharedPreferences prefs;
    String robotAddress;

    EditTextPreference robotAddressPref, webCamAddressPref, speakerAddressPref;
    ListPreference intervalPref, delayPref, gaitPref;
    CheckBoxPreference debugMode;

    Boolean debug;

    String TAG = "PreferenceFrag";

    public Preference() {
        // Required empty public constructor
    }


    @Override
     public void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.preference);

        prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());

        robotAddressPref = (EditTextPreference) findPreference("RobotAddress");
        webCamAddressPref = (EditTextPreference) findPreference("webCamAddress");
        speakerAddressPref = (EditTextPreference) findPreference("speakerAddress");

        intervalPref = (ListPreference) findPreference("kine_set_interval");
        delayPref = (ListPreference) findPreference("kine_set_delay");
        gaitPref = (ListPreference) findPreference("kine_set_gait_point_delay");

        debugMode = (CheckBoxPreference) findPreference("DebugMode");

        robotAddressPref.setSummary(prefs.getString("RobotAddress", " "));
        webCamAddressPref.setSummary(prefs.getString("webCamAddress", " "));
        speakerAddressPref.setSummary(prefs.getString("speakerAddress", " "));

        robotAddressPref.setOnPreferenceChangeListener(this);
        webCamAddressPref.setOnPreferenceChangeListener(this);
        speakerAddressPref.setOnPreferenceChangeListener(this);

        intervalPref.setOnPreferenceChangeListener(this);
        delayPref.setOnPreferenceChangeListener(this);
        gaitPref.setOnPreferenceChangeListener(this);

        debug = prefs.getBoolean("DebugMode", false);

        if (debug) Log.i(TAG, "OnCreate");
     }

    public boolean onPreferenceChange(android.preference.Preference preference, Object newValue) {
        String value = newValue.toString();
        debug = prefs.getBoolean("DebugMode", false);

        if ("RobotAddress".equals(preference.getKey())){
            preference.setSummary(value);

        } else if ("webCamAddress".equals(preference.getKey())){
            preference.setSummary(value);

        } else if ("speakerAddress".equals(preference.getKey())){
            preference.setSummary(value);

        } else if("kine_set_interval".equals(preference.getKey())) {
            int intValue = Integer.parseInt(value);
            setPreference("kine_set_interval", intValue);
            if (debug) Log.i(TAG, "[onPreferenceChange] kine_set_interval + intValue");
        } else if("kine_set_delay".equals(preference.getKey())) {
            int intValue = Integer.parseInt(value);
            setPreference("kine_set_delay", intValue);
            if (debug) Log.i(TAG, "[onPreferenceChange] kine_set_delay + intValue");
        } else if("kine_set_gait_point_delay".equals(preference.getKey())) {
            int intValue = Integer.parseInt(value);
            setPreference("kine_set_gait_point_delay", intValue);
            if (debug) Log.i(TAG, "[onPreferenceChange] kine_set_gait_point_delay + intValue");
        }

        return true;
    }


    public void setPreference (final String methodCall, final int value) {
        robotAddress = prefs.getString("RobotAddress", " ");
        debug = prefs.getBoolean("DebugMode", false);

        Thread thread=new Thread(new Runnable()
        {
            @Override
            public void run()
            {
                try {
                    XMLRPCClient client = new XMLRPCClient(new URL(robotAddress));
                    client.call(methodCall, value);

                    if (debug) {
                        Log.d(TAG, "[setPreference] Connection to " + robotAddress + " is successful.");
                        Log.d(TAG, "[setPreference] " + methodCall + " " + value + " ");
                    }
                    getActivity().runOnUiThread(new Runnable() {
                        public void run()
                        {
                            Toast.makeText(getActivity(), "[SetPreference] " + methodCall +
                                    " of value " + value + " successful.", Toast.LENGTH_SHORT).show();
                        }
                    });
                } catch(Exception ex) {
                    Log.e(TAG, "[setPreference] " + methodCall + " " + value + " ");
                    Log.e(TAG, ex.toString());
                    getActivity().runOnUiThread(new Runnable() {
                        public void run()
                        {
                            Toast.makeText(getActivity(), "[SetPreference] " + methodCall +
                                    " of value " + value + " unsuccessful.", Toast.LENGTH_SHORT).show();
                        }
                    });
                }
            }

        });
        thread.start();
    }

}
