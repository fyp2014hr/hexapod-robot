package bryce.fyp2015.hrcontroller;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import java.net.URL;

import de.timroes.axmlrpc.XMLRPCClient;
import it.neokree.materialnavigationdrawer.MaterialNavigationDrawer;


public class MainActivity extends MaterialNavigationDrawer {

    boolean onPick = false, autoMode = false, servoOff = false, danceMode = false, demoMode = false;
    String robotAddress, speakerAddress;
    Menu menu;

    public void init(Bundle savedInstanceState) {

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        robotAddress = prefs.getString("RobotAddress", " ");
        speakerAddress = prefs.getString("speakerAddress", " ");

        checkConnection();


        // set the header image
        this.setDrawerHeaderImage(R.drawable.mat2);

        // create sections
        this.addSection(newSection("Basic Mode", new BasicMode()));
        this.addSection(newSection("Dance Mode", new TiltMode()));
        this.addSection(newSection("Immersive Mode", new Intent(this, ImmersiveMode.class)));

//        this.addDivisor();
//        this.addSection(newSection("Gallery", R.drawable.ic_action_picture, new MainActivity()));
//        this.addBottomSection(newSection("Help",R.drawable.ic_action_help,new Preference()));
//        this.addSection(newSection("Help", R.drawable.ic_mic_white_24dp,new FragmentButton()).setSectionColor(Color.parseColor("#9c27b0")));
//        this.addSection(newSection("Section",R.drawable.ic_hotel_grey600_24dp,new FragmentButton()).setSectionColor(Color.parseColor("#03a9f4")));

        // create bottom section
        this.addBottomSection(newSection("Settings",R.drawable.ic_action_settings,new Preference()));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        this.menu = menu;
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        } else if (id == R.id.clamp) {
            if (!onPick) {
                setMovement("pcc_tight", -999);
                item.setTitle("Release");
            } else{
                setMovement("pcc_release", -999);
                item.setTitle("Pick");
            }
            onPick = !onPick;
            return true;
        } else if (id == R.id.autoMode){

            if (!autoMode) {
                setMovement("kine_set_action" , 13);
                item.setTitle("Manual");
            } else{
                setMovement("kine_set_action" , 1);
                item.setTitle("Auto");
            }
            autoMode = !autoMode;

            return true;
        } else if (id == R.id.servoOff){

            if (!servoOff) {
                setMovement("servo_power_off" , -999);
                item.setTitle("Servo On");
            } else{
                setMovement("servo_power_on" , -999);
                item.setTitle("Servo Off");
            }
            servoOff = !servoOff;

            return true;
        } else if (id == R.id.danceMode){

            if (!danceMode) {
                setMovement("kine_set_action" , 14);
                item.setTitle("Dance Mode Off");
            } else{
                setMovement("kine_set_action" , 1);
                item.setTitle("Dance Mode");
            }
            danceMode = !danceMode;

            return true;
        } else if (id == R.id.demo){

            if (!demoMode) {
                setMovement("kine_set_action" , 15);
                item.setTitle("Demo Off");
            } else{
                setMovement("kine_set_action" , 1);
                item.setTitle("Demo");
            }
            demoMode = !demoMode;

            return true;
        }



        return super.onOptionsItemSelected(item);
    }

    public void changeAutoTitle(){
        MenuItem autoItem = menu.findItem(R.id.autoMode);
        if (autoMode) {
            autoItem.setTitle("Auto");
            autoMode = !autoMode;
        }

    }

    public void checkConnection(){
        Thread thread=new Thread(new Runnable()
        {
            @Override
            public void run()
            {
                try {
                    XMLRPCClient client = new XMLRPCClient(new URL(robotAddress));
                    client.call("servo_power_on");
                    client.call("kine_set_action", 1);

                    XMLRPCClient speakClient = new XMLRPCClient(new URL(speakerAddress));
                    speakClient.call("speak", "Robot is Ready");

                    Log.i("XML-RPC: ", "Connection to " + robotAddress + " is successful.");
                } catch(Exception ex) {
                    Log.e("XML-RPC Address: ", robotAddress);
                    Log.e("XML-RPC Fail: ", ex.toString());
                }
            }

        });
        thread.start();
    }



    public void setMovement (final String command, final int direction) {

        Thread thread=new Thread(new Runnable()
        {
            @Override
            public void run()
            {
                try {
                    XMLRPCClient client = new XMLRPCClient(new URL(robotAddress));
                    if (direction==-999){
                        client.call(command);
                    } else {
                        client.call(command, direction);
                    }
                    Log.i("Set Position ", direction + "");
                } catch(Exception ex) {
                    Log.e("[Failed] Set Position ", direction +"");
                }
            }

        });
        thread.start();
    }
}
