package bryce.fyp2015.hrcontroller;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.support.v4.app.NavUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;

import bryce.fyp2015.hrcontroller.Joystick.DualJoystickView;
import bryce.fyp2015.hrcontroller.Joystick.JoystickMovedListener;
import bryce.fyp2015.hrcontroller.MJPEGStream.MjpegInputStream;
import bryce.fyp2015.hrcontroller.MJPEGStream.MjpegView;
import bryce.fyp2015.hrcontroller.util.SystemUiHider;
import de.timroes.axmlrpc.XMLRPCClient;


/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 *
 * @see SystemUiHider
 */
public class ImmersiveMode extends Activity {
    /**
     * Whether or not the system UI should be auto-hidden after
     * {@link #AUTO_HIDE_DELAY_MILLIS} milliseconds.
     */
    private static final boolean AUTO_HIDE = true;

    /**
     * If {@link #AUTO_HIDE} is set, the number of milliseconds to wait after
     * user interaction before hiding the system UI.
     */
    private static final int AUTO_HIDE_DELAY_MILLIS = 3000;

    /**
     * If set, will toggle the system UI visibility upon interaction. Otherwise,
     * will show the system UI visibility upon interaction.
     */
    private static final boolean TOGGLE_ON_CLICK = true;

    /**
     * The flags to pass to {@link SystemUiHider#getInstance}.
     */
    private static final int HIDER_FLAGS = SystemUiHider.FLAG_HIDE_NAVIGATION;

    /**
     * The instance of the {@link SystemUiHider} for this activity.
     */
    private SystemUiHider mSystemUiHider;

    SharedPreferences prefs;
    String robotAddress, webCamAddress, speakerAddress, speakerText;

    private static final String TAG = "ImmersiveMode";
    private MjpegView mv;
    private boolean suspending = false, activityStop = false, debug;
    private SeekBar heightSeek;
    private TextView heightReset, ultraSoundReading;
    private int currentAction;
    private XMLRPCClient client, speakerClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_immersive_mode);
        setupActionBar();
        setupSharedPreference();
        setupConnection();
        setupSeekBar();
        setupCamera();
        setupJoyStick();
        ultraReading();
        setClockwiseButton();
        setupUIHider();

    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);

        // Trigger the initial hide() shortly after the activity has been
        // created, to briefly hint to the user that UI controls
        // are available.
        delayedHide(100);
    }

    public void onPause() {
        Log.e(TAG,"onPause()");
        super.onPause();
        activityStop = true;
        if(mv!=null){
            if(mv.isStreaming()){
                mv.stopPlayback();
                suspending = true;
            }
        }
    }

    public void onResume() {
        Log.e(TAG,"onResume()" + suspending);
        super.onResume();
        activityStop = false;

        if(mv!=null){
            if(suspending){
                Log.e(TAG,"onResume()" + suspending + "2");
                new DoRead().execute(webCamAddress);
                suspending = false;
            }
        }

    }

    public void onStop() {
        Log.e(TAG,"onStop()");
        activityStop = true;
        super.onStop();
    }

    public void onDestroy() {
        Log.e(TAG,"onDestroy()" + (mv == null));
        activityStop = true;
        super.onDestroy();
    }


    /**
     * Set up the {@link android.app.ActionBar}, if the API is available.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    private void setupActionBar() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            // Show the Up button in the action bar.
            getActionBar().setDisplayHomeAsUpEnabled(true);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            // This ID represents the Home or Up button. In the case of this
            // activity, the Up button is shown. Use NavUtils to allow users
            // to navigate up one level in the application structure. For
            // more details, see the Navigation pattern on Android Design:
            //
            // http://developer.android.com/design/patterns/navigation.html#up-vs-back
            //
            // TODO: If Settings has multiple levels, Up should navigate up
            // that hierarchy.
            NavUtils.navigateUpFromSameTask(this);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.immersive, menu);
        return super.onCreateOptionsMenu(menu);
    }

    //
    // Load SharedPreference Value
    //

    public void setupSharedPreference(){
        prefs = PreferenceManager.getDefaultSharedPreferences(this);
        debug = prefs.getBoolean("DebugMode", false);
        robotAddress = prefs.getString("RobotAddress", " ");
        webCamAddress = prefs.getString("webCamAddress", "http://192.168.3.1:9002/") + "?action=stream";
        speakerAddress = prefs.getString("speakerAddress", " ");
    }

    //
    // Following functions are connection-related.
    // i.e. XML-RPC related
    //

    public void setupConnection(){
        Thread thread=new Thread(new Runnable()
        {
            @Override
            public void run()
            {
                try {
                    client = new XMLRPCClient(new URL(robotAddress));
                    setMovement(1);
                    client.call("servo_power_on");

                    if (debug) Log.d(TAG, "[setupConnection] Connection to " + robotAddress + " is successful.");

                    speakerClient = new XMLRPCClient(new URL(speakerAddress));
                    speakerClient.call("speak", "Robot is Ready");

                    if (debug) Log.d(TAG, "[setupConnection] Connection to " + speakerAddress + " is successful.");
                } catch(Exception ex) {
                    Log.e(TAG, "[setupConnection] unsuccessful");
                    Log.e(TAG, ex.toString());
                }
            }

        });
        thread.start();
    }

    public void setMovement(final int value) {
        setMovement("kine_set_action", value);
    }

    public void setMovement(final String methodCall, final int value) {
        Thread thread=new Thread(new Runnable()
        {
            @Override
            public void run()
            {
                try {
                    XMLRPCClient client = new XMLRPCClient(new URL(robotAddress));
                    if (value==-999){
                        client.call(methodCall);
                    } else {
                        client.call(methodCall, value);
                    }
                    if (debug) Log.d(TAG, "[setMovement] " + methodCall + " " + value + " ");
                } catch(Exception ex) {
                    Log.e(TAG, "[setMovement] " + methodCall + " " + value + " ");
                    Log.e(TAG, ex.toString());
                }
            }

        });
        thread.start();
    }

    public void setDoubleMovement (final String methodCall, final double value) {
        Thread thread=new Thread(new Runnable()
        {
            @Override
            public void run()
            {
                try {
                    XMLRPCClient client = new XMLRPCClient(new URL(robotAddress));
                    client.call(methodCall , value);
                    if (debug) Log.d(TAG, "[setDoubleMovement] " + methodCall + " " + value + " ");
                } catch(Exception ex) {
                    Log.e(TAG, "[setDoubleMovement] " + methodCall + " " + value + " ");
                    Log.e(TAG, ex.toString());
                }
            }

        });
        thread.start();

    }

    //
    // Following functions are camera-related.
    //

    public void setupCamera() {
        // Set up the MJPEGView
        mv = (MjpegView) findViewById(R.id.mv);

        Log.e(TAG, webCamAddress);
        if(!suspending){
            new DoRead().execute(webCamAddress);
            suspending = false;
            if (debug) Log.d(TAG, "[setupCamera] Camera is set successfully");
        }

    }

    public class DoRead extends AsyncTask<String, Void, MjpegInputStream> {
        protected MjpegInputStream doInBackground(String... url) {
            //TODO: if camera has authentication deal with it and don't just not work
            HttpResponse res;
            DefaultHttpClient httpclient = new DefaultHttpClient();
            if (debug) Log.d(TAG, "[SetCamera] 1. Sending http request");
            try {
                res = httpclient.execute(new HttpGet(URI.create(url[0])));
                if (debug) Log.d(TAG, "[SetCamera] 2. Request finished, status = " + res.getStatusLine().getStatusCode());
                if(res.getStatusLine().getStatusCode()==401){
                    //You must turn off camera User Access Control before this will work
                    return null;
                }
                return new MjpegInputStream(res.getEntity().getContent());
            } catch (ClientProtocolException e) {
                e.printStackTrace();
                Log.e(TAG, "[SetCamera] Request failed-ClientProtocolException");
                Log.e(TAG, e.toString());
                //Error connecting to camera
            } catch (IOException e) {
                e.printStackTrace();
                Log.e(TAG, "[SetCamera] Request failed-IOException");
                Log.e(TAG, e.toString());
                //Error connecting to camera
            }

            return null;
        }

        protected void onPostExecute(MjpegInputStream result) {
            mv.setSource(result);
            if(result!=null) {
                result.setSkip(1);
            }
            mv.setDisplayMode(MjpegView.SIZE_BEST_FIT);
            mv.showFps(true);
        }
    }

    //
    // Setting the SeekBar
    //

    public void setupSeekBar() {
        heightSeek = (SeekBar) findViewById(R.id.heightSeek);
        heightReset = (TextView) findViewById(R.id.heightReset);
        heightSeek.setOnSeekBarChangeListener(_listenerH);
        findViewById(R.id.heightReset).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                heightSeek.setProgress(10);
                heightReset.setHint("-30");
                setMovement(6);
                setDoubleMovement("kine_set_posZ", -30);
            }
        });
        if (debug) Log.d(TAG, "[setupSeekBar] SeekBar is set successfully");
    }

    private SeekBar.OnSeekBarChangeListener _listenerH = new SeekBar.OnSeekBarChangeListener() {

        int progressChanged = 0;

        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser){
            progressChanged = progress;
        }

        public void onStartTrackingTouch(SeekBar seekBar) {
            //
        }

        public void onStopTrackingTouch(SeekBar seekBar) {
            heightReset.setText("Height: " + (progressChanged - 40));
            setMovement(6);
            setDoubleMovement("kine_set_posZ", (progressChanged - 40));
        }
    };

    //
    // Setup JoyStick
    //

    //JoyStick Implementation

    DualJoystickView sJoyStick;
    private int radiusL, angleL;
    private double radiusR, angleR;
    final static double positionMax = 30.0;

    public void setupJoyStick(){
        ultraSoundReading = (TextView) findViewById(R.id.ultraSoundReading);
        sJoyStick = (DualJoystickView) findViewById(R.id.joystickTranslate);
        sJoyStick.setOnJostickMovedListener(_listenerL, _listenerR);
        currentAction = 1;
        if (debug) Log.d(TAG, "[setupJoyStick] JoyStick is set successfully");
    }

    private JoystickMovedListener _listenerL = new JoystickMovedListener() {

        public void OnMoved(int pan, int tilt) {
            if (AUTO_HIDE) {
                delayedHide(AUTO_HIDE_DELAY_MILLIS);
            }
            radiusL = (int) Math.sqrt((pan*pan) + (tilt*tilt));
            angleL = (int) ((Math.atan2(-pan, -tilt)) * 180 / Math.PI);
            currentAction = 1;

            if (radiusL > 5) {
                if (angleL > -22.5 && angleL <= 22.5) {
                    setMovement(2);
                    if (debug) Log.d(TAG, "[JoyStickL] Forward");
                } else if (angleL > 22.5 && angleL <= 67.5) {
                    setMovement(9);
                    if (debug) Log.d(TAG, "[JoyStickL] Forward Left");
                } else if (angleL > 67.5 && angleL <= 112.5) {
                    setMovement(4);
                    if (debug) Log.d(TAG, "[JoyStickL] Left");
                } else if (angleL > 112.5 && angleL <= 157.5) {
                    setMovement(11);
                    if (debug) Log.d(TAG, "[JoyStickL] Backward Left");
                } else if (angleL > 157.5 || angleL < -157.5) {
                    setMovement(3);
                    if (debug) Log.d(TAG, "[JoyStickL] Backward");
                } else if (angleL < -22.5 && angleL >= -67.5) {
                    setMovement(10);
                    if (debug) Log.d(TAG, "[JoyStickL] Forward Right");
                } else if (angleL < -67.5 && angleL >= -112.5) {
                    setMovement(5);
                    if (debug) Log.d(TAG, "[JoyStickL] Right");
                } else if (angleL < -112.5 && angleL >= -157.5) {
                    setMovement(12);
                    if (debug) Log.d(TAG, "[JoyStickL] Backward Right");
                }
            }

        }

        public void OnReleased() {

            if (AUTO_HIDE) {
                delayedHide(AUTO_HIDE_DELAY_MILLIS);
            }
            if (debug) Log.d(TAG, "[JoyStickL] On Release");
        }

        public void OnReturnedToCenter() {
            if (AUTO_HIDE) {
                delayedHide(AUTO_HIDE_DELAY_MILLIS);
            }
            setMovement(1);
            if (debug) Log.d(TAG, "[JoyStickL] Return to Central: 1st Try");
            try{
                Thread.sleep(300);
            } catch (Exception ex) {
                Log.w("Sleep", ex.toString());
            }
            setMovement(1);
            if (debug) Log.d(TAG, "[JoyStickL] Return to Central: 2nd Try");
        }
    };

    private JoystickMovedListener _listenerR = new JoystickMovedListener() {

        public void OnMoved(int pan, int tilt) {
            if (AUTO_HIDE) {
                delayedHide(AUTO_HIDE_DELAY_MILLIS);
            }
            radiusR = Math.sqrt((pan*pan) + (tilt*tilt));
            angleR = Math.atan2(-pan, -tilt);
            if (radiusR > 5) {
                if ( currentAction != 6)
                    setMovement(6);
                positionMovement(Math.min(radiusR, 10), angleR * 180 / Math.PI + 90);
                currentAction = 6;
            }
        }

        public void OnReleased() {
            if (AUTO_HIDE) {
                delayedHide(AUTO_HIDE_DELAY_MILLIS);
            }
            if (debug) Log.d(TAG, "[JoyStickR] On Release");
            positionMovement(0, 0);
        }

        public void OnReturnedToCenter() {
            if (AUTO_HIDE) {
                delayedHide(AUTO_HIDE_DELAY_MILLIS);
            }
            positionMovement(0, 0);
            if (debug) Log.d(TAG, "[JoyStickR] Return to Central: 1st Try");
            try{
                Thread.sleep(300);
            } catch (Exception ex) {
                Log.w(TAG, "[Sleep] " + ex.toString());
            }
            positionMovement(0, 0);
            if (debug) Log.d(TAG, "[JoyStickR] Return to Central: 2nd Try");
        }
    };

    public void positionMovement(double radius, double angle){
        if (debug) Log.d(TAG, "[positionMovement] Radius: "+ radius + "; Angle: "+angle);
        double x = radius * Math.cos(Math.toRadians(angle));
        double y = radius * Math.sin(Math.toRadians(angle));

        setDoubleMovement("kine_set_posY", x / 8 * positionMax);
        setDoubleMovement("kine_set_posX", (-1) * y / 8 * positionMax);
    }

    //
    // Setup Ultrasoud
    //

    Thread ultraThread;

    public void ultraReading() {
        ultraSoundReading = (TextView) findViewById(R.id.ultraSoundReading);
        ultraThread = new Thread(new Runnable()
        {
            @Override
            public void run()
            {
                while (!activityStop) {
                    if (debug) {
                    java.util.Date now = new java.util.Date();
                    Log.d(TAG, "[UltraReading Start] " + new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(now));
                    }
                    try {
                        XMLRPCClient client = new XMLRPCClient(new URL(robotAddress));

                        final double readings = (double) client.call("ultrasound_sensor_read");
                        if (readings > 0) {
                            runOnUiThread(new Runnable() {
                                public void run() {
                                    ultraSoundReading.setText("Distance: " + String.format("%.0f", readings));
                                    //connectStatusText.setTextColor(getResources().getColor(R.color.success_connect));
                                }
                            });
                        }

                    } catch (Exception ex) {
                        Log.w(TAG, "[UltraReading] " + ex.toString());
                    }

                    try {
                        Thread.sleep(1000);
                        //if (debug) {
                        java.util.Date now2 = new java.util.Date();
                        Log.d(TAG, "[UltraReading End] " + new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(now2));
                        //}
                    } catch (Exception ex) {
                        Log.w(TAG, "[UltraReading] " + ex.toString());
                    }
                }
            }

        });
        ultraThread.start();
        Log.e(TAG,"ultraReading start");
    }

    public void ultraStop() {
        Log.d(TAG, "[ultraStop] Start!");
        if(ultraThread!=null){
//            boolean retry = true;
//            while(retry) {
//                try {
//                    ultraThread.join();
//                    retry = false;
//                } catch (InterruptedException e) {
//                    Log.e(TAG, "[stopPlayback] Error in Thread joining");
//                    Log.e(TAG, e.toString());
//                }
//            }
            ultraThread = null;
        }
        Log.d(TAG, "[ultraStop] Completed!");

    }

    //
    // Setup Screen Capture
    //


    public void setScreenCapture(){
        findViewById(R.id.pink_icon).setOnClickListener(screenCapture);
    }

    View.OnClickListener screenCapture = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            Thread thread=new Thread(new Runnable()
            {
                @Override
                public void run()
                {
                    //Toast.makeText(getActivity(), "Clicked pink Floating Action Button", Toast.LENGTH_SHORT).show();
                    try {
                        saveImageToExternalStorage(getBitmapFromURL(prefs.getString("webCamAddress", "http://192.168.3.1:9002/") + "?action=snapshot"));
                    } catch (Exception ex) {
                        Log.e("Screen Capture", "failed");
                        Log.e("Screen Capture", "" + ex);
                    }
                }

            });
            thread.start();
        }
    };

    public static Bitmap getBitmapFromURL(String src) {
        try {
            URL url = new URL(src);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap myBitmap = BitmapFactory.decodeStream(input);
            return myBitmap;
        } catch (IOException e) {
            e.printStackTrace();
            Log.e("Screen Capture", e.toString());
            return null;
        }
    }

    public boolean saveImageToExternalStorage(Bitmap image) throws IOException{
        final String APP_PATH_SD_CARD = "/HexaPodControl/screenshot";
        final String fullPath = Environment.getExternalStorageDirectory().getAbsolutePath() + APP_PATH_SD_CARD;

        final SimpleDateFormat sdFormat = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");
        final Date current = new Date();
        //System.out.println(sdFormat.format(current));

        Log.e("saveToExternalStorage()", "1");
        try {
            File dir = new File(fullPath);
            Log.e("saveToExternalStorage()", "2");
            if (!dir.exists()) {
                dir.mkdirs();
                Log.e("saveToExternalStorage()", "2.5");
            }

            OutputStream fOut = null;
            File file = new File(fullPath, "snapshot"+sdFormat.format(current)+".png");
            file.createNewFile();
            fOut = new FileOutputStream(file);
            Log.e("saveToExternalStorage()", "3");

            // 100 means no compression, the lower you go, the stronger the compression
            image.compress(Bitmap.CompressFormat.PNG, 100, fOut);
            fOut.flush();
            fOut.close();
            Log.e("saveToExternalStorage()", "4");

            MediaStore.Images.Media.insertImage(getContentResolver(), file.getAbsolutePath(), file.getName(), file.getName());

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                Intent mediaScanIntent = new Intent(
                        Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
                Uri contentUri = Uri.fromFile(file); //out is your output file
                mediaScanIntent.setData(contentUri);
                sendBroadcast(mediaScanIntent);
            } else {
                sendBroadcast(new Intent(
                        Intent.ACTION_MEDIA_MOUNTED,
                        Uri.parse("file://" + Environment.getExternalStorageDirectory())));
            }
            Log.e("saveToExternalStorage()", "Done!");
            runOnUiThread(new Runnable() {
                public void run() {
                    Toast.makeText(ImmersiveMode.this, "Screen Captured as " + fullPath + "snapshot" + sdFormat.format(current) + ".png",
                            Toast.LENGTH_SHORT).show();
                }
            });

            return true;

        } catch (Exception e) {
            Log.e("saveToExternalStorage()", e.getMessage());
            return false;
        }
    }

    //
    // Setup SystemUiHider
    //

    private void setupUIHider(){
        final View controlsView = findViewById(R.id.fullscreen_content_controls);
        final View contentView = findViewById(R.id.fullscreen_content);

        // Set up an instance of SystemUiHider to control the system UI for
        // this activity.
        mSystemUiHider = SystemUiHider.getInstance(this, contentView, HIDER_FLAGS);
        mSystemUiHider.setup();
        mSystemUiHider
                .setOnVisibilityChangeListener(new SystemUiHider.OnVisibilityChangeListener() {
                    // Cached values.
                    int mControlsHeight;
                    int mShortAnimTime;

                    @Override
                    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
                    public void onVisibilityChange(boolean visible) {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
                            // If the ViewPropertyAnimator API is available
                            // (Honeycomb MR2 and later), use it to animate the
                            // in-layout UI controls at the bottom of the
                            // screen.
                            if (mControlsHeight == 0) {
                                mControlsHeight = controlsView.getHeight();
                            }
                            if (mShortAnimTime == 0) {
                                mShortAnimTime = getResources().getInteger(
                                        android.R.integer.config_shortAnimTime);
                            }
                            controlsView.animate()
                                    .translationY(visible ? 0 : mControlsHeight)
                                    .setDuration(mShortAnimTime);
                        } else {
                            // If the ViewPropertyAnimator APIs aren't
                            // available, simply show or hide the in-layout UI
                            // controls.
                            controlsView.setVisibility(visible ? View.VISIBLE : View.GONE);
                        }

                        if (visible && AUTO_HIDE) {
                            // Schedule a hide().
                            delayedHide(AUTO_HIDE_DELAY_MILLIS);
                        }
                    }
                });

        // Set up the user interaction to manually show or hide the system UI.
        contentView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (TOGGLE_ON_CLICK) {
                    mSystemUiHider.toggle();
                } else {
                    mSystemUiHider.show();
                }
            }
        });

        // Upon interacting with UI controls, delay any scheduled hide()
        // operations to prevent the jarring behavior of controls going away
        // while interacting with the UI.
        findViewById(R.id.fullscreen_content_controls).setOnTouchListener(mDelayHideTouchListener);
//        findViewById(R.id.rotateClockwise).setOnTouchListener(mDelayHideTouchListener);
//        findViewById(R.id.rotateAnticlockwise).setOnTouchListener(mDelayHideTouchListener);
        findViewById(R.id.joystickTranslate).setOnTouchListener(mDelayHideTouchListener);
        findViewById(R.id.heightSeek).setOnTouchListener(mDelayHideTouchListener);
    }

    public void setClockwiseButton(){
        setTouchListener(R.id.rotateClockwise, 8);
        setTouchListener(R.id.rotateAnticlockwise, 7);
    }

    public void setTouchListener (final int viewID, final int movement) {
        findViewById(viewID).setOnTouchListener(new View.OnTouchListener() {


            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (AUTO_HIDE) {
                    delayedHide(AUTO_HIDE_DELAY_MILLIS);
                }

                switch(event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        setMovement(movement);
//                        view.findViewById(viewID).setBackgroundColor(getResources().getColor(R.color.primary_light));
//                        return false; // if you want to handle the touch event

                    case MotionEvent.ACTION_UP:
                        setMovement(1);
//                        view.findViewById(viewID).setBackgroundColor(getResources().getColor(R.color.primary));
//                        return false; // if you want to handle the touch event
                }
                return true;
            }
        });
    }

    /**
     * Touch listener to use for in-layout UI controls to delay hiding the
     * system UI. This is to prevent the jarring behavior of controls going away
     * while interacting with activity UI.
     */
    View.OnTouchListener mDelayHideTouchListener = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View view, MotionEvent motionEvent) {
            if (AUTO_HIDE) {
                delayedHide(AUTO_HIDE_DELAY_MILLIS);
            }
            return false;
        }
    };

    Handler mHideHandler = new Handler();
    Runnable mHideRunnable = new Runnable() {
        @Override
        public void run() {
            mSystemUiHider.hide();
        }
    };

    /**
     * Schedules a call to hide() in [delay] milliseconds, canceling any
     * previously scheduled calls.
     */
    private void delayedHide(int delayMillis) {
        mHideHandler.removeCallbacks(mHideRunnable);
        mHideHandler.postDelayed(mHideRunnable, delayMillis);
    }
}
