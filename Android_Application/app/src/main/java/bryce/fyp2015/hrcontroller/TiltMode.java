package bryce.fyp2015.hrcontroller;


import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.rey.material.widget.Button;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;

import bryce.fyp2015.hrcontroller.Joystick.JoystickMovedListener;
import bryce.fyp2015.hrcontroller.Joystick.JoystickView;
import bryce.fyp2015.hrcontroller.MJPEGStream.MjpegInputStream;
import bryce.fyp2015.hrcontroller.MJPEGStream.MjpegView;
import de.timroes.axmlrpc.XMLRPCClient;

/**
 * A simple {@link Fragment} subclass.
 */
public class TiltMode extends Fragment implements SensorEventListener {

    SharedPreferences prefs;
    String robotAddress, webCamAddress;
    boolean listenerToggleOn;

    private static final String TAG = "MjpegActivity";
    private MjpegView mv;
    private View view;
    private TextView ultraSoundReading;

    double[] result = new double[2];
    int rotSensitivity = 75;
    volatile boolean activityStopped = false, suspending = false;
    Switch toggle;

    public TiltMode() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        view = inflater.inflate(R.layout.fragment_tilt_mode, container, false);
        Log.d(TAG, "OnCreateView");
        loadRobotAddress();
        checkConnection();

        // Inflate the layout for this fragment
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Log.d(TAG, "OnCreate");
        loadView();
        setupCamera();
        ultraReading();
        setScreenCapture();
        clearResult();
        setRotationZSeek();

        m_sensorManager = (SensorManager) getActivity().getSystemService(Context.SENSOR_SERVICE);
        registerListeners();
        listenerToggleOn = true;
        tiltModeRotation();

        view.findViewById(R.id.pink_icon).setOnClickListener(screenCapture);

        toggle = (Switch) view.findViewById(R.id.dancingSwitch);
        toggle.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    // The toggle is enabled
                    Log.w(TAG, "setToggle");
                    registerListeners();
                    listenerToggleOn = true;
                    Toast.makeText(getActivity(), "Registered Listener", Toast.LENGTH_SHORT).show();

                } else {
                    // The toggle is disabled
                    unregisterListeners();
                    listenerToggleOn = false;
                    Toast.makeText(getActivity(), "Shut Down Listener", Toast.LENGTH_SHORT).show();
                }
            }
        });

        Log.d(TAG, "OnCreate2");
    }

    @Override
    public void onDestroy() {
        if (listenerToggleOn) {
            unregisterListeners();
            listenerToggleOn = false;
        }
        setMovement("kine_set_action", 0);
        activityStopped = true;
        ultraStop();
        super.onDestroy();
    }

    @Override
    public void onResume() {
        if (!listenerToggleOn) {
            registerListeners();
            listenerToggleOn = true;
        }
        if(mv!=null){
            if(suspending){
                webCamAddress = prefs.getString("webCamAddress", "http://192.168.4.2:9002/?action=stream") + "?action=stream";
                Log.e(TAG,"onResume()" + suspending + "2");
                new DoRead().execute(webCamAddress);
                suspending = false;
            }
        }
        setMovement("kine_set_action", 6);
        activityStopped = false;
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        activityStopped = true;
        ultraStop();
        if(mv!=null){
            if(mv.isStreaming()){
                mv.stopPlayback();
                suspending = true;
            }
        }
        if (listenerToggleOn) {
            unregisterListeners();
            listenerToggleOn = false;
        }
    }


    public void loadView(){
        ultraSoundReading = (TextView) view.findViewById(R.id.ultrasoundReading);
    }

    /*
    The following code should be refactor because they appear in every activity, fragment where
    connection to robot and webCam is required.
    */

    TextView rotZReading;

    SeekBar rotationZSeek;

    public void setRotationZSeek(){
        rotationZSeek = (SeekBar) view.findViewById(R.id.rotZSeek);
        rotationZSeek.setOnSeekBarChangeListener(_listenerR);
        rotZReading = (TextView) view.findViewById(R.id.rotZValue);
        view.findViewById(R.id.rotZText).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rotationZSeek.setProgress(15);
                rotZReading.setHint("Default (0)");
                setMovement("kine_set_action", 6);
                setDoubleMovement("kine_set_rotZ", 0);
            }
        });
    }

    private SeekBar.OnSeekBarChangeListener _listenerR = new SeekBar.OnSeekBarChangeListener() {

        int progressChanged = 0;

        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser){
            progressChanged = progress;
            toggle.setChecked(false);
        }

        public void onStartTrackingTouch(SeekBar seekBar) {
            // TODO Auto-generated method stub
        }

        public void onStopTrackingTouch(SeekBar seekBar) {
            //Toast.makeText(getActivity(),"seek bar progress:"+ (progressChanged - 60),
            //Toast.LENGTH_SHORT).show();

            if ((progressChanged - 15) == 0) {
                rotZReading.setHint("" + (progressChanged - 15));
            } else {
                rotZReading.setHint("" + (progressChanged - 15));
            }
            setMovement("kine_set_action", 6);
            setDoubleMovement("kine_set_rotZ", (progressChanged - 15));
        }
    };


    // Following functions are connection-related.
    public void loadRobotAddress(){
        prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());
        robotAddress = prefs.getString("RobotAddress", "http://");
    }

    public void checkConnection(){
        final TextView connectStatusText = (TextView) view.findViewById(R.id.ConnectionStatus);
        Thread thread=new Thread(new Runnable()
        {
            @Override
            public void run()
            {
                try {
                    XMLRPCClient client = new XMLRPCClient(new URL(robotAddress));

//                    setMovement(6);
                    client.call("servo_power_on");
                    client.call("set_kine_action", 6);


                    Log.i("XML-RPC: ", "Connection to " + robotAddress + " is successful.");
                    getActivity().runOnUiThread(new Runnable() {
                        public void run()
                        {
                            //Toast.makeText(getActivity(), "Connection Successful.", Toast.LENGTH_LONG).show();
                            connectStatusText.setText("Connection Successful.");
                            connectStatusText.setTextColor(getResources().getColor(R.color.success_connect));
                        }
                    });
                } catch(Exception ex) {
                    Log.e("XML-RPC Address: ", robotAddress);
                    Log.e("XML-RPC Fail: ", ex.toString());
                }
            }

        });
        thread.start();
    }

    public void resetPosition() {
        // Reset the initial position of the robot
        setDoubleMovement("kine_set_posX", 0);
        setDoubleMovement("kine_set_posY", 0);

        setDoubleMovement("kine_set_rotX", 0);
        setDoubleMovement("kine_set_rotY", 0);
        setDoubleMovement("kine_set_rotZ", 0);
    }

    // Following functions are camera-related.

    public void setupCamera() {
        // Set up the MJPEGView
        mv = (MjpegView) view.findViewById(R.id.mv);
        prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());
        webCamAddress = prefs.getString("webCamAddress", "http://192.168.4.2:9002/?action=stream") + "?action=stream";

        Log.e(TAG, webCamAddress);
//        The following is sample IP camera address for testing purpose
//        webCamAddress = "http://cascam.ou.edu/axis-cgi/mjpg/video.cgi?resolution=320x240";
        if(!suspending){
            new DoRead().execute(webCamAddress);
            suspending = false;
        }
    }

    public class DoRead extends AsyncTask<String, Void, MjpegInputStream> {
        protected MjpegInputStream doInBackground(String... url) {
            HttpResponse res;
            DefaultHttpClient httpclient = new DefaultHttpClient();
//            Log.d(TAG, "1. Sending http request");
            try {
                res = httpclient.execute(new HttpGet(URI.create(url[0])));
                Log.d(TAG, "2. Request finished, status = " + res.getStatusLine().getStatusCode());
                if(res.getStatusLine().getStatusCode()==401){
                    //You must turn off camera User Access Control before this will work
                    return null;
                }
                return new MjpegInputStream(res.getEntity().getContent());
            } catch (ClientProtocolException e) {
                e.printStackTrace();
                Log.d(TAG, "Request failed-ClientProtocolException", e);
                //Error connecting to camera
            } catch (IOException e) {
                e.printStackTrace();
                Log.d(TAG, "Request failed-IOException", e);
                //Error connecting to camera
            }

            return null;
        }

        protected void onPostExecute(MjpegInputStream result) {
            mv.setSource(result);
            mv.setDisplayMode(MjpegView.SIZE_BEST_FIT);
            mv.showFps(true);
        }
    }

    // Following the part of Gyroscope

    /* sensor data */
    SensorManager m_sensorManager;
    float []m_lastMagFields;
    float []m_lastAccels;
    private float[] m_rotationMatrix = new float[16];
    private float[] m_orientation = new float[4];

    /* fix random noise by averaging tilt values */
    final static int AVERAGE_BUFFER = 30;
    float m_lastPitch = 0.f;
    float m_lastYaw = 0.f;
    float m_lastRoll = 0.f;

    private void registerListeners() {
        Log.w(TAG, "registerListeners");
        m_sensorManager.registerListener(this, m_sensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD), SensorManager.SENSOR_DELAY_GAME);
        m_sensorManager.registerListener(this, m_sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER), SensorManager.SENSOR_DELAY_GAME);
    }

    private void unregisterListeners() {
        m_sensorManager.unregisterListener(this);
        clearResult();
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
            accel(event);
        }
        if (event.sensor.getType() == Sensor.TYPE_MAGNETIC_FIELD) {
            mag(event);
        }
    }

    private void accel(SensorEvent event) {
        if (m_lastAccels == null) {
            m_lastAccels = new float[3];
        }

        System.arraycopy(event.values, 0, m_lastAccels, 0, 3);
    }

    private void mag(SensorEvent event) {
        if (m_lastMagFields == null) {
            m_lastMagFields = new float[3];
        }

        System.arraycopy(event.values, 0, m_lastMagFields, 0, 3);

        if (m_lastAccels != null) {
            computeOrientation();
        }
    }

    Filter [] m_filters = { new Filter(), new Filter(), new Filter() };

    private class Filter {
        static final int AVERAGE_BUFFER = 10;
        float []m_arr = new float[AVERAGE_BUFFER];
        int m_idx = 0;

        public float append(float val) {
            m_arr[m_idx] = val;
            m_idx++;
            if (m_idx == AVERAGE_BUFFER)
                m_idx = 0;
            return avg();
        }
        public float avg() {
            float sum = 0;
            for (float x: m_arr)
                sum += x;
            return sum / AVERAGE_BUFFER;
        }

    }

    private double[] computeOrientation() {

        if (SensorManager.getRotationMatrix(m_rotationMatrix, null, m_lastAccels, m_lastMagFields)) {
            SensorManager.getOrientation(m_rotationMatrix, m_orientation);

            /* 1 radian = 57.2957795 degrees
             * [0] : yaw, rotation around z axis     - Moving
             * [1] : pitch, rotation from Forward to Backward (RotY)
             * [2] : roll, rotation from Leftward to Rightward (RotX)
             * it features normal rectangular coordinate plane setting */
            float yaw = m_orientation[0];
            float pitch = m_orientation[1];
            float roll = m_orientation[2];

            m_lastYaw = m_filters[0].append(yaw);
            m_lastPitch = m_filters[1].append(pitch);
            m_lastRoll = m_filters[2].append(roll);

            result[0] = returnDegree(m_lastRoll);
            result[1] = returnDegree(m_lastPitch);
            return result;
        }

        result[0] = 0;
        result[1] = 0;

        return result;
    }

    final static double rotationMax = 10.0;

    public double returnDegree (double degrees){

        if (degrees > 0) {
            if (degrees > 1.5)
                return rotationMax ;
            return   degrees / 2.2 * rotationMax ;
        } else if (degrees < 0) {
            if (degrees < -1.5)
                return -rotationMax ;
            return   degrees / 2.2 * rotationMax;
        } else if (degrees == 0){
            return 0;
        }

        return -1;
    }

    public void clearResult(){
        result[0] = 0;
        result[1] = 0;
    }

    public void tiltModeRotation() {
        Thread thread=new Thread(new Runnable()
        {
            @Override
            public void run()
            {
                while (!activityStopped) {
                    if (listenerToggleOn) {
                        // To prevent toppling for maximum
                        if (result[0] > 12.0 && result[1] > 12.0 ) {
                            setDoubleMovement("kine_set_rotX", 11.0);
                            setDoubleMovement("kine_set_rotY", 11.0);
                        } else {
                            setDoubleMovement("kine_set_rotX", returnDegree(result[0]));
                            setDoubleMovement("kine_set_rotY", returnDegree(result[1]));
                        }

                        try {
                            Thread.sleep(rotSensitivity);
                        } catch (Exception ex) {
                            Log.w("Tilt Mode", ex.toString());
                        }
                    }
                }
            }

        });

        thread.start();
    }

    //
    // This is ultraThread
    //

    Thread ultraThread;

    public void ultraReading() {
        ultraSoundReading = (TextView) view.findViewById(R.id.ultrasoundReading);
        ultraThread = new Thread(new Runnable()
        {
            @Override
            public void run()
            {
                while (!activityStopped) {
                    //if (debug) {
                    java.util.Date now = new java.util.Date();
                    Log.d(TAG, "[UltraReading Start] " + new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(now));
                    //}
                    try {
                        XMLRPCClient client = new XMLRPCClient(new URL(robotAddress));

                        final double readings = (double) client.call("ultrasound_sensor_read");
                        if (readings > 0) {
                            getActivity().runOnUiThread(new Runnable() {
                                public void run() {
                                    ultraSoundReading.setText("Distance: " + String.format("%.0f", readings));
                                    //connectStatusText.setTextColor(getResources().getColor(R.color.success_connect));
                                }
                            });
                        }

                    } catch (Exception ex) {
                        Log.w(TAG, "[UltraReading] " + ex.toString());
                    }

                    try {
                        Thread.sleep(1000);
                        //if (debug) {
                        java.util.Date now2 = new java.util.Date();
                        Log.d(TAG, "[UltraReading End] " + new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(now2));
                        //}
                    } catch (Exception ex) {
                        Log.w(TAG, "[UltraReading] " + ex.toString());
                    }
                }
            }

        });
        ultraThread.start();
        Log.e(TAG,"ultraReading start");
    }

    public void ultraStop() {
        Log.d(TAG, "[ultraStop] Start!");
        if(ultraThread!=null){
            ultraThread = null;
        }
        Log.d(TAG, "[ultraStop] Completed!");

    }


    public void setMovement (final String methodCall, final int direction) {

        Thread thread=new Thread(new Runnable()
        {
            @Override
            public void run()
            {
                try {
                    XMLRPCClient client = new XMLRPCClient(new URL(robotAddress));

                    client.call(methodCall , direction);

                    Log.i("set Movement ", methodCall +"");
                    Log.i("Set Position ", direction +"");

                } catch(Exception ex) {
                    //TODO: to change for d to w after debug
                    Log.d("[Failed] Set Position ", direction +"");

                }
            }

        });
        thread.start();
    }

    public void setDoubleMovement (final String methodCall, final double direction) {

        Thread thread=new Thread(new Runnable()
        {
            @Override
            public void run()
            {
                try {
                    XMLRPCClient client = new XMLRPCClient(new URL(robotAddress));

                    client.call(methodCall , direction);

                    Log.i("set Movement ", methodCall +"");
                    Log.i("Set Position ", direction +"");

                } catch(Exception ex) {
                    //TODO: to change for d to w after debug
                    Log.i("F:set Movement ", methodCall +"");
                    Log.i("F:Set Position ", direction +"");

                }
            }

        });
        thread.start();
    }

    public void setScreenCapture(){
        view.findViewById(R.id.pink_icon).setOnClickListener(screenCapture);
    }

    View.OnClickListener screenCapture = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            Thread thread=new Thread(new Runnable()
            {
                @Override
                public void run()
                {
                    //Toast.makeText(getActivity(), "Clicked pink Floating Action Button", Toast.LENGTH_SHORT).show();
                    try {
                        saveImageToExternalStorage(getBitmapFromURL(prefs.getString("webCamAddress", "http://192.168.3.1:9002/") + "?action=snapshot"));
                    } catch (Exception ex) {
                        Log.e("Screen Capture", "failed");
                        Log.e("Screen Capture", "" + ex);
                    }
                }

            });
            thread.start();
        }
    };

    public static Bitmap getBitmapFromURL(String src) {
        try {
            URL url = new URL(src);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap myBitmap = BitmapFactory.decodeStream(input);
            return myBitmap;
        } catch (IOException e) {
            e.printStackTrace();
            Log.e("Screen Capture", e.toString());
            return null;
        }
    }

    public boolean saveImageToExternalStorage(Bitmap image) throws IOException{
        final String APP_PATH_SD_CARD = "/HexaPodControl/screenshot";
        final String fullPath = Environment.getExternalStorageDirectory().getAbsolutePath() + APP_PATH_SD_CARD;

        final SimpleDateFormat sdFormat = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");
        final Date current = new Date();
        //System.out.println(sdFormat.format(current));

        Log.e("saveToExternalStorage()", "1");
        try {
            File dir = new File(fullPath);
            Log.e("saveToExternalStorage()", "2");
            if (!dir.exists()) {
                dir.mkdirs();
                Log.e("saveToExternalStorage()", "2.5");
            }

            OutputStream fOut = null;
            File file = new File(fullPath, "snapshot"+sdFormat.format(current)+".png");
            file.createNewFile();
            fOut = new FileOutputStream(file);
            Log.e("saveToExternalStorage()", "3");

            // 100 means no compression, the lower you go, the stronger the compression
            image.compress(Bitmap.CompressFormat.PNG, 100, fOut);
            fOut.flush();
            fOut.close();
            Log.e("saveToExternalStorage()", "4");

            MediaStore.Images.Media.insertImage(getActivity().getContentResolver(), file.getAbsolutePath(), file.getName(), file.getName());

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                Intent mediaScanIntent = new Intent(
                        Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
                Uri contentUri = Uri.fromFile(file); //out is your output file
                mediaScanIntent.setData(contentUri);
                getActivity().sendBroadcast(mediaScanIntent);
            } else {
                getActivity().sendBroadcast(new Intent(
                        Intent.ACTION_MEDIA_MOUNTED,
                        Uri.parse("file://" + Environment.getExternalStorageDirectory())));
            }
            Log.e("saveToExternalStorage()", "Done!");
            getActivity().runOnUiThread(new Runnable() {
                public void run() {
                    Toast.makeText(getActivity(), "Screen Captured as " + fullPath + "snapshot" + sdFormat.format(current) + ".png",
                            Toast.LENGTH_SHORT).show();
                }
            });

            return true;

        } catch (Exception e) {
            Log.e("saveToExternalStorage()", e.getMessage());
            return false;
        }
    }

}